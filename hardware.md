
### [全频带阻塞干扰](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMzE2OTQyNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMzE2OTQyNA==)

[:camera_flash:【2023-10-31 09:13:58】](https://mp.weixin.qq.com/s?__biz=MzIzMzE2OTQyNA==&mid=2648956484&idx=1&sn=07ae0ab62a6eb4e6c3e804384f4cc04f&chksm=f09ec93bc7e9402d8bc9c7be8ee46378c91e0b8074064b1da3220c2b9637c236ebd2b87f8cf5&scene=27#wechat_redirect)

坚持以非正常体位探寻未知信号。 国内商业安全、TSCM技术反窃密、隐私保护及物理安全领域前行者。持久关注通信安全、海外威胁情报、犯罪防御与群氓效应。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_685fcc00c91c" alt="" />

---


### [物联网IoT安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMjcxMzg5Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMjcxMzg5Mg==)

[:camera_flash:【2023-09-22 18:02:20】](https://mp.weixin.qq.com/s?__biz=MzUzMjcxMzg5Mg==&mid=2247488663&idx=1&sn=de989a22f0410cc2fafb7e835448b0cd&chksm=faae4c5ccdd9c54a0ddb2eac70151be6050dd42f87fa548bd7267efa979a0168d3cc5fd1d90b&scene=27#wechat_redirect)

我们是一个专注于物联网IOT安全 固件逆向 近源攻击 硬件破解的公众号，与我们一起学习进步。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a86356973ade" alt="" />

---


### [Gh0xE9](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNTc5MTMyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNTc5MTMyNg==)

[:camera_flash:【2023-10-23 19:24:02】](https://mp.weixin.qq.com/s?__biz=MzAwNTc5MTMyNg==&mid=2247499112&idx=1&sn=0a0012f2bee27f2014249014ff9a1fef&chksm=9b15f659ac627f4f42f9b19a44d8007ca67dc7570cfa945df535c234392620982afa2ea1cba6&scene=27#wechat_redirect)

做勾八安全，卷的要死

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3953fa56d850" alt="" />

---


### [取证杂谈](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3Mjc0MjkwMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3Mjc0MjkwMQ==)

[:camera_flash:【2023-09-05 20:43:21】](https://mp.weixin.qq.com/s?__biz=MzI3Mjc0MjkwMQ==&mid=2247484143&idx=1&sn=8c1d201e8e602959caee208c12df0bdc&chksm=eb2ca275dc5b2b6309335c158bddf10300f779c7c32ed91045714dcb1d6e48858a1fa3a8ddc4&scene=27#wechat_redirect)

分享电子取证及司法鉴定相关知识

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3cc4dcce2d29" alt="" />

---


### [电子取证及可信应用协创中心](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxODA3NDc3NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxODA3NDc3NA==)

[:camera_flash:【2023-08-28 10:47:56】](https://mp.weixin.qq.com/s?__biz=MzAxODA3NDc3NA==&mid=2247485445&idx=1&sn=bb06ac570ab65d114e7945244f0b0a51&chksm=9bda95fcacad1cea7196826e36b07a06a8541fd67a0f56d95fe4288f646716bb2af3bee3276e&scene=27#wechat_redirect)

电子取证及可信应用湖北省协同创新中心（培育）的订阅号。介绍和宣传法庭科学中的电子数据取证（Digital Forensics）涉及的科学知识、技术实践和创新应用，推动电子数据取证的普及和发展。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_16885e328471" alt="" />

---


### [湘雪尘奕](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MzE3NzczNQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MzE3NzczNQ==)

[:camera_flash:【2023-10-20 12:50:47】](https://mp.weixin.qq.com/s?__biz=MzI2MzE3NzczNQ==&mid=2650622939&idx=1&sn=ae813bc956a5451716fbcf18e5301420&chksm=f2b638b3c5c1b1a5ec9035267bc951bb63de6505defbb6515cea789d5be63ead0ce39d85a7ae&scene=27#wechat_redirect)

最近公众号主要以视频为主，每周发布一个新的视频！内容主要围绕工业控制系统网络内容，主要涉及工控设备、协议解析及检测评估等方向。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9a02359365f9" alt="" />

---


### [电子物证](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNDcwMDgzMA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNDcwMDgzMA==)

[:camera_flash:【2023-10-30 09:57:36】](https://mp.weixin.qq.com/s?__biz=MzAwNDcwMDgzMA==&mid=2651046141&idx=2&sn=12c06ca2091524628a35619882d2b5f9&chksm=80d08f0cb7a7061a6ef585fd83e47d1ad2fe5022df89dc91bac8596049d7092b0f598d22c746&scene=27#wechat_redirect)

关注刑事证据科学前沿发展，传递电子物证技术最新趋势

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f624bd447b0f" alt="" />

---


### [数据安全与取证](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNzU0NjIyMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNzU0NjIyMg==)

[:camera_flash:【2023-10-27 09:42:53】](https://mp.weixin.qq.com/s?__biz=MzIyNzU0NjIyMg==&mid=2247488024&idx=1&sn=4da783b052fc7a2493203eeb57ade76f&chksm=e85ed719df295e0f4a2c9253f6572fb8616e0bc17a49b5dc7e0fa2c51b57f3a65df7ee832752&scene=27#wechat_redirect)

我们专注于电子取证技术的分享及司法鉴定实践的交流，涵盖了计算机取证、手机取证、电子物证、司法鉴定等众多领域，是电子取证公司与电子取证从业者交流学习的首选平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_336d761a246b" alt="" />

---


### [网安杂谈](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwMTMzMDUwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwMTMzMDUwNg==)

[:camera_flash:【2023-10-29 09:08:10】](https://mp.weixin.qq.com/s?__biz=MzAwMTMzMDUwNg==&mid=2650887948&idx=1&sn=13e60cd78cc2c7cda1393dc6f06e1f8b&chksm=812eab29b659223fe4bb34462bf2b5c75224d921a06abac7c6d12418642e47b7e692d6434b68&scene=27#wechat_redirect)

关注电子数据取证与网络犯罪调查

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_901b6c524273" alt="" />

---


### [电子数据取证与鉴定](http://wechat.doonsec.com/wechat_echarts/?biz=MzA4NTAzOTI4OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA4NTAzOTI4OA==)

[:camera_flash:【2019-08-19 23:46:52】](https://mp.weixin.qq.com/s?__biz=MzA4NTAzOTI4OA==&mid=2649978561&idx=1&sn=5bf7daf3cfa9021c62edc14647e7cb73&chksm=87d9d716b0ae5e001f99315b42522ffda59daf8b2be3d5cbd63e71df52d4e9496c54ab558d74&scene=27&key=4905a5bb167a5cf43f6bd9858b58b7d0727d6055d0a0f0aebddeed8ebd8980d74a5d9fb182140ba625cb683b916621046aea42035e7c09ff8af96166c5cbeeb900d37bc1712377ae73deda740f582ed2898af4d6e0b42ee159c39d64814f48255f11c1d0084c4b97f29c966b0887e9409a5046aa9b532118067ff96c471bf674&ascene=0&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6303050f&lang=zh_CN&exportkey=AxEnapnH8TsVZznWZZttOTk%3D&pass_ticket=&scene=27#wechat_redirect)

介绍电子数据取证与鉴定的基础知识、法律法规、技术标准及检验方法、检验流程、取证和鉴定的工具等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a3bb8fead1f9" alt="" />

---


### [IOTsecZone](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MTY5NjQzMA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MTY5NjQzMA==)

[:camera_flash:【2022-10-24 17:40:16】](https://mp.weixin.qq.com/s?__biz=Mzg4MTY5NjQzMA==&mid=2247487389&idx=1&sn=fd60d2c8d60fe92c643c30b4ea8a1dab&chksm=cf63445cf814cd4a1f6dd518563a841736fdda62a59e01c5fd7106f1aab57a57a2112ec8ae78&scene=27#wechat_redirect)

极客的专属潮酷聚集地，尽在iotsec-zone

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3f7435f8d16d" alt="" />

---


### [卫星黑客](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxMjExNDMzNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxMjExNDMzNw==)

[:camera_flash:【2023-08-30 09:05:50】](https://mp.weixin.qq.com/s?__biz=MzIxMjExNDMzNw==&mid=2247484029&idx=1&sn=38f1d8d9155e7cc84b5b9103960d6f2d&chksm=974a4cb3a03dc5a5603b0390b3300f38d56057a994ebe559919b4ae758965f2cf4b02af392ba&scene=27#wechat_redirect)

中国卫星黑客

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_2beea18480d6" alt="" />

---


### [小火箭](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NjAyMTg1NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NjAyMTg1NQ==)

[:camera_flash:【2020-11-03 22:29:06】](https://mp.weixin.qq.com/s?__biz=MzI1NjAyMTg1NQ==&mid=2649509452&idx=1&sn=b5c59654f29b11cf8b82efaec413e7dc&chksm=f235ea7cc542636afde5ef7c9d912fad7523ea4b5e3f7c5adaf2b33ab0b216268ce9f42a25de&scene=58&subscene=0&key=c184f4d6bd516cbc386c113dd32c0bb51d172c2c5a1958ed4edc1b59955fb11c7d81fd1679b83955d6ba98ba33e212520604671ae346144b8a3c32c6ebf0606ec3c0e07d342d02a4242c7d2ecf2d8aabfbd8764d746fc73f2cfb5a64692882a84749d574add344ade452002582fc3243919c1cd430148c9822a62e45fb3c4803&ascene=1&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=A7mYSvYvFsMDy90AAI2P2KI%3D&a&scene=27#wechat_redirect)

全球航空航天业内人员和深度爱好者们的交流基地。内容由邢强博士一人情怀创制。 小火箭没有体制背景，没有商业运营，唯一可以依赖的就是你啦！ 小火箭出品，必属精品！我已加入“维权骑士”(rightknights.com)的版权保护计划。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_762feeded5aa" alt="" />

---


### [胖猴实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MDQ5MzA4NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MDQ5MzA4NA==)

[:camera_flash:【2023-05-31 18:30:29】](https://mp.weixin.qq.com/s?__biz=MzI1MDQ5MzA4NA==&mid=2247484079&idx=1&sn=8b8fb5144b612b10af544b8eb26ec6b0&chksm=e980235cdef7aa4a797610e923ef7867641ca3a130b23aa81c8f68deb467599e1c2a1e431abf&scene=27#wechat_redirect)

一群想胖的猴子

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1623eeae8094" alt="" />

---


### [IoVSecurity](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2MDk1Nzg2MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2MDk1Nzg2MQ==)

[:camera_flash:【2023-10-31 19:08:44】](https://mp.weixin.qq.com/s?__biz=MzU2MDk1Nzg2MQ==&mid=2247595941&idx=3&sn=c8fe707d7360ae3b0bcb0b5580c0d048&chksm=fc03276fcb74ae798fbe4a1937785cd51cba591be3e83731d8e7dc669fc66b6dfa764e71278d&scene=27#wechat_redirect)

为智能网联汽车行业专业人士提供信息和网络安全技术、产品及服务等行业发展最新咨询

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_469225414d82" alt="" />

---


### [TahirSec](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzNjIwMzM5Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzNjIwMzM5Nw==)

[:camera_flash:【2023-10-23 20:30:18】](https://mp.weixin.qq.com/s?__biz=MzkzNjIwMzM5Nw==&mid=2247487348&idx=1&sn=f3274662c6b73e3ab5962b7ce1b12e30&chksm=c2a30a9ef5d48388b12936c54aa49766d6dac992ad72297dbc9fb568b2c610a6943cb5e6ad5f&scene=27#wechat_redirect)

未知攻，焉知防，专注于高级威胁研究与数字调查取证。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ea4772086043" alt="" />

---


### [IoT物联网技术](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTA4MzA0MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTA4MzA0MA==)

[:camera_flash:【2023-10-30 19:28:50】](https://mp.weixin.qq.com/s?__biz=MjM5OTA4MzA0MA==&mid=2454931785&idx=1&sn=bcbac2e103b2a940b37f96be11614ae3&chksm=b167040686108d108c656a49a647cb18bc3129285281c09a89207ca2b07b269682700f22cd3c&scene=27#wechat_redirect)

云计算、物联网、大模型暗中观察小分队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5617c1b503e7" alt="" />

---


### [360IoT安全](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MjE4NzY2NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MjE4NzY2NA==)

[:camera_flash:【2021-07-15 11:48:54】](https://mp.weixin.qq.com/s?__biz=Mzg2MjE4NzY2NA==&mid=2247483838&idx=1&sn=fa08c987bab29872139a14618b80f28c&chksm=ce0af841f97d715766117050091f686cb9036eb7a894ed2b54f27a69dcc2f38ccf28d6389015&scene=27&key=a21e8dc39c4f19e97f15dd060e9638b2341110968a6c452fe9059550448a3141a702358eef8bae326cf3d2f6c8c1935e6faa7f7470785b937347632391550efd8e78300e2d1a29e8f056943759dee411b251d8ff49e4b6fec9d6ceb4a2e3707166ccdbd61585533c91980f72088edd45b238694b273e127e1f02ecee0b1b2ef9&ascene=0&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=AwWSQUvlz7bdW5HkknWSRKQ%3D&acctmode=0&pass_ticket=lMd8ch8hI4cLWZ55%2BNmVe%2FVY3RfFydUJ3ipGrpixVuBuix6OlZcebzII%2BKnCe%2F8v&wx_header=0&fontgear=2&scene=27#wechat_redirect)

360 IoT安全研究院

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b700ad8d4775" alt="" />

---


### [移动安全王铁头](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjE4NDgyMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjE4NDgyMg==)

[:camera_flash:【2022-03-09 11:53:57】](https://mp.weixin.qq.com/s?__biz=MzkzMjE4NDgyMg==&mid=2247485985&idx=1&sn=d186bb5fd06755b6eac6350b8d35623d&chksm=c25eddb8f52954aeb06d84f2cd7dc1a79f1f79574598a25385d8108cd66324b1461c3d2efc87&scene=27&key=da5527f6ccd6edd791be5d4f75d5606aae9f2a0f1c6b0ff5eb6424a9483a1833f915a3497245ef41b248ce372d2e80a9699a959e802c4c492e8a1195462a531112fcf479c445941efe3063b839400b5eedbc4132b615996e7edffc10ab5e6cf4b970c9716388af84f8d6decfbfab0723d5f8a1e13e4097244aa5fa3b4666342c&ascene=0&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=AWKmYMB%2B4j6mMSOpO357Rp8%3D&acctmode=0&pass_ticket=CLipu1oc3Xo23kKaFPk9VMmJWr0KzXLDKmtoNd6o2PRzCklLCrUb3XxUITQ9X3B0&wx_header=0&fontgear=2&scene=27#wechat_redirect)

专注移动安全和 IOT安全，希望和大家一起成长.

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_219d99d38ff9" alt="" />

---


### [第一汽车攻防实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1OTMwNDE3OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1OTMwNDE3OA==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

关注汽车网络安全，定期发布最专业的汽车网络安全攻防实验室相关实验结果，公开一些汽车网络攻防实验的细节，公布发现的汽车安全漏洞以帮助生产厂家认识到安全隐患！针对联网化的汽车网络安全性提供研究测试和公开报告！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_85e019862246" alt="" />

---


### [信睿网络](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MDA2NjExMA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MDA2NjExMA==)

[:camera_flash:【2023-06-20 07:37:36】](https://mp.weixin.qq.com/s?__biz=Mzg4MDA2NjExMA==&mid=2247489475&idx=1&sn=bb74300170df02eea1be3a8da1d8619e&chksm=cf7bbc92f80c3584edb85145ebda4bac6bafdb5a14dc152d4678314d9912eced69b9a3445762&key=4b8e18db99a662cf32295e31069165170046c6a556849a50523859c438f85e2947c2d560c1f128ec2c591f864cefdc1b4d5e75aa58fe8ce67500299cfb04167fb6b2d3193891bdf08671d05f9482727975737b96698c755ea98d7819c709999046b3f5e38003400fb760a5103aec82b734334a68db76319cbc1212edeee6d3d8&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_18cae47da613&countrycode=AL&exportkey=n_ChQIAhIQ7hyokl5%2FdmjRyzOyAtJo5BLuAQIE97dBBAEAAAAAAOgJCqM4G4wAAAAOpnltbLcz9gKNyK89dVj0wDWFo%2B9gLL8s2fZ6Jszk95QvPPLtmzFhJt02JteTK0%2B4CHBYudUry6aHqZkMsCA6wIvMGJVCdZqA3t5YObPf8pNUA%2F5wATTsbpra51lFGUyV4ux%2FaPWqQr8lJCmtM%2FGbLN8vHpg2pDBxhx2%2FZcs1DflQCqewa2pYaYjp6gYeB1KVXtSm4%2BQv3Vw1y1lOdruQNwQ1m%2F3uB3OcQxGuyFA8V%2BLcqAjiU9%2FVJ6NiqsNNoIrHKVkSGNFewlkMMn0Pyb%2BuBSVHSSd5RGw%3D&acctmode=0&pass_ticket=gi4zatG1r7&scene=27#wechat_redirect)

吉林省信睿网络官方公众号

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_18cae47da613" alt="" />

---


### [网络安全自学](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3NzQ3NzY4OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3NzQ3NzY4OA==)

[:camera_flash:【2023-10-04 18:03:54】](https://mp.weixin.qq.com/s?__biz=MzI3NzQ3NzY4OA==&mid=2247484015&idx=1&sn=1ad56945711f4c3f8b87230bbdf23479&chksm=eb64e238dc136b2e9764b7abac1634255c016ddd947832c1feb60dd1ac0336121debe5d0b2c9&scene=27#wechat_redirect)

网络安全自学交流，提高效率，少走弯路

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1d8521f5d38e" alt="" />

---


### [网络安全与取证研究](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NTU3NTY0Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NTU3NTY0Nw==)

[:camera_flash:【2023-10-31 08:01:39】](https://mp.weixin.qq.com/s?__biz=Mzg3NTU3NTY0Nw==&mid=2247488143&idx=1&sn=cfd9caef9634af5477758dff9a0bdb49&chksm=cf3e32bff849bba96884052245e17f4602d96075c5446f188b8a93d8b68f398938aa4992083f&scene=27#wechat_redirect)

旨于开展网络安全相关法律法规研究与电子数据取证技术研究，分享小知识、小技巧，共同学习提高，推进技术规范化建设。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0aa8f6bfaffd" alt="" />

---


### [汽车网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMzM3NTM3MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMzM3NTM3MA==)

[:camera_flash:【2022-02-17 18:02:00】](https://mp.weixin.qq.com/s?__biz=MzIzMzM3NTM3MA==&mid=2247484849&idx=1&sn=4c5d1e1e09520b235d8f1978d0ba6f2d&chksm=e887d654dff05f42255f95069ffa5378369382db11e0e1f1669152a6422f60e2ec9b00d7a5f6&scene=27&key=c472ddfeba91422ed8b45841ed8b1a99e5ac58290823940ba12223d577240231bd25546ce409c117aa31cf7c43cc369219d004dcd4296dcfa61596a52d26b643025b40e1ef595107cb439eb0ecbf3de051228daf9beb2215c27c2c11a0713fa94222fc8ea919df943651888c5ce762110e3fb663ef19a87f5af51f4c5bd3a12d&ascene=15&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6307001e&lang=zh_CN&session_us=gh_ff13bad322ea&exportkey=Ac7SKvtBKl3fAABUEg0SvDk%3D&acctmode=0&pass_ticket=sKNK3ur%2Bb1y%2F5r54bUQpdAiUD6mFZMhVBFbNRVyVltYCv4RpXfE08XwnQ7CIHFlc&wx_header=0&fontgear=2&scene=27#wechat_redirect)

汽车联网化的已经在业界获得高度共识，联网后的”网络安全“正在成为热点；本公众号是第一个关注汽车网络安全的自媒体，通过原创文章，发布车联网安全相关的重大事件、技术突破及发展趋势；为关注汽车网络安全的个人或组织提供一个重要的资讯通道！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_60224803d00e" alt="" />

---


### [天问实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwNzI0ODE5NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwNzI0ODE5NQ==)

[:camera_flash:【2023-05-16 16:53:35】](https://mp.weixin.qq.com/s?__biz=MzkwNzI0ODE5NQ==&mid=2247486126&idx=1&sn=9469b964abdf6305e01ab0555d0102d8&chksm=c0dd5d82f7aad4940bdee8e960ad62d514c32c33038961f5730656f0a9a3b7cb3f509496d08b&scene=27#wechat_redirect)

杭州安恒天问实验室，专注于物联网、车联网、工控等新兴领域的安全测试与安全研究

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5b8a6bf277a4" alt="" />

---


### [太空安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwNjM4NTg4OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwNjM4NTg4OQ==)

[:camera_flash:【2023-10-28 10:06:10】](https://mp.weixin.qq.com/s?__biz=MzkwNjM4NTg4OQ==&mid=2247494014&idx=1&sn=01835e47d29002d18117eb844ad5490a&chksm=c0ebe9f0f79c60e602b904ca2ea59b854739ca2590b122735c247243909bfe2e182b54b20538&scene=27#wechat_redirect)

学习卫星互联网，研究卫星通信安全！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_37ba12d49c79" alt="" />

---

