
### [OWASP](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTk5NDMyMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTk5NDMyMw==)

[:camera_flash:【2023-10-23 17:44:54】](https://mp.weixin.qq.com/s?__biz=MjM5OTk5NDMyMw==&mid=2652098703&idx=1&sn=d9d3667ca905dcd0dcab70131069d043&chksm=bcd586338ba20f25ed2eebc2e1f325833df82b881d6ccd684ed8130e3f242d3282ae237ddd34&scene=0&xtrack=1&key=debaf56e0df4198f20de1741d07db4119f23832dbc6ed466edd1c8cfaca614e24d031e562feb3cd05bcdce6144d9c795d31cb003e2ed91737fe0db057c0f767589596484608091b5054c78e3ba67d7173ed53bce5adb6bf3804a0c260617b4f19ae785960fd7d3b10ad5250a7daadbe6a757db74ac706e1007d7cb358216289c&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_6977ebcf9804&countrycode=AL&exportkey=n_ChQIAhIQZVKDVGsRoG5HUGJf9kHrFBLuAQIE97dBBAEAAAAAAFZ7NDyG7qcAAAAOpnltbLcz9gKNyK89dVj0EX3K1XxLo3P3SjQfjyiOyBF4dp6%2BQY0VbKBSD2SUtUtygPRR6IA5QiDeM2q8vlqPjqdA7uE2cYgTpqWSIjn0AMSbnEnuMV6ao4%2Fxj%2Fh785C4G%2BjT1z1Ix9U00JavtekOTGgjm2KvKs7lSsMnmaKDk0cuExcGd45kODI6rjFRUiQ%2B%2BzSu%2FiiWB8sGlzd3sGNw%2B07yPWWKoF%2Buo0B0NCSNeZi4C52YjYJP9hr05oZnA%2FZnV7RFprHSK9Kji6PcxN8BznGbylmdy3Y%3D&acctmode=0&pass_ticket=Z0rYv0Cn5gHDoc%2BgylTu728XjWnFD1ZGQ0hk0vetFRITeU7ndTWTPapo6Q6UU2hj&wx_header=0&fontgear=2&scene=27#wechat_redirect)

OWASP中国，SecZone互联网安全研究中心官方平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_6977ebcf9804" alt="" />

---


### [安全乐观主义](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5Mzg3NTUwNQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5Mzg3NTUwNQ==)

[:camera_flash:【2022-08-25 18:44:20】](https://mp.weixin.qq.com/s?__biz=MzA5Mzg3NTUwNQ==&mid=2447805135&idx=1&sn=99ceb8cacd82370085f5b7e6ced48f83&chksm=84451691b3329f87054c5a4547a2b3d3e71ee8125bb3bc09aacfec7c0a34ce35b9527b08644b&scene=27#wechat_redirect)

实践分享企业在建设安全开发生命周期各阶段及流程中的优秀实践，内容涉及代码审计、业界对标、系统工程化心得、国外资料分享，搭建应用安全交流平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d6239d0bb816" alt="" />

---


### [甲方安全建设](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0MDcyMTMxOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0MDcyMTMxOQ==)

[:camera_flash:【2023-03-01 15:30:16】](https://mp.weixin.qq.com/s?__biz=MzU0MDcyMTMxOQ==&mid=2247487310&idx=1&sn=4910260a58f50825bb8546419fdfce8c&chksm=fb35a686cc422f900cd52ab79276e5d0a5c5b73124e6aa52c178c688c2626931b5e8545b2c11&scene=27#wechat_redirect)

甲方安全建设的点滴，共同学习，一起进步。 笔耕不辍也是对自我的督促。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_97cc04db2203" alt="" />

---


### [i安全梦](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxODY0NTYwMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxODY0NTYwMw==)

[:camera_flash:【2023-03-03 22:30:51】](https://mp.weixin.qq.com/s?__biz=MzAxODY0NTYwMw==&mid=2247485771&idx=1&sn=81f430c751dd3a777c94a3e85efae3d7&chksm=ab88c18e49a749e5eb7380123e23e90ccbb0bae1b62ecdde464a7bf8c457dc9209e224f6fa59&scene=27&key=47ecebb9f9a1a9ab9568083421dc05f6bed3849ceee31447c6ec920e0f2fe181a44e19aa10f69248cd145df88c10bea8567f1c22a1c7cfd2275f8e56e0b89d8ecc02e7f31c65adade62361037c06c4553a999722e725f500516f893659f60d9b6b8b17c19fbc8bd1ed2cfea771ef13ad0e0957790e7256bac163e6b71e59145f&ascene=15&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_c3a017ea74d6&countrycode=AL&exportkey=n_ChQIAhIQhO12GjSH2GoHwRs%2BPvR73xLuAQIE97dBBAEAAAAAADenIw9525QAAAAOpnltbLcz9gKNyK89dVj0%2BGFpNMVzQF8TeC3a3cVyvky5d%2Bh9wwfq0BRazdeJ6WyH6G9ltNl1NfSMEfiDy6TTyvJfHI61E0K3qmD0NsMdlfGIOpIzADOk8Y88sjSDcFIT36Mv%2BiO02U2LLxU0plWUsw%2FLa%2BHXZBJ5unpBG9Ff4vWLtSIrLqwCdiYXVZF0oyoljBDPaVky0n%2F38e%2FJDl122V6mt0X5ImUKpKYbQu5UCkbIfUdAdhrdcpcHve5fkjPlWlf8nbxbOsmPQdofWoxGSJ6FPTabjX0%3D&acctmode=0&pass_ticket=x88psTQ83&scene=27#wechat_redirect)

共同探讨企业网信实践经验，解决企业网信建设最后一公里。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e7945c695dd2" alt="" />

---


### [鸟哥谈安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNzg2Njg1MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNzg2Njg1MA==)

[:camera_flash:【2022-12-27 00:01:09】](https://mp.weixin.qq.com/s?__biz=MzAxNzg2Njg1MA==&mid=2652071241&idx=1&sn=0371340ee8871e7c3924939140913dcb&chksm=8038cc44b74f45528f44a185176ee53c5069e25c39b20cbf8113a7b7855da790511b5d0f3c2c&scene=27&key=81a3de90d40f41c9626ec9b684c17d2831f612d5a8504cc19007e17ff93c647cb5d2293d122b30488bbb932bcec00697501273310873b7233bb7a3a1aaff373f38bca2fb61dc197b034740f8b6f169a049e98e79757a3f874f636d30dc7f45b98c81534a70e9c8291bcc56c9087c6d6b151032ef1290b15d25989a20f8b76ba1&ascene=15&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6308011a&lang=zh_CN&session_us=gh_b273ce95df95&exportkey=n_ChQIAhIQjaQ7QMR1bBwCVRqxGX5v4xL5AQIE97dBBAEAAAAAAHy7CUN%2FvdkAAAAOpnltbLcz9gKNyK89dVj0Tz5%2Bq1ovH7w1siA4ZTPiVbmVl7Hg5XW7GRIqxKcxiDPbXF7XQTAE9KsPKmxLPyRF%2BC0hAlHmYf0%2BHMZchkUXxuKMdZnybND3eCo9so%2BLrSf3m5%2BfUhpi%2FhBvOib0f3ktzreSSBz%2B5GPFaG%2BrDW0cXhuPtN17ddlVApDqoemh3nAY7BrBnt184MwoNrV0Vhd3WRkWlpI%2Fzzq34y8PH4tobzB2XPtoRWQX2CDq0t0c9Mw2Bjuu6YMF8H%2BrHIMSP1VnBAzvCBK0sUDUgKX3i17UJVdk3Q%3D%3D&acctmode&scene=27#wechat_redirect)

全面深入解读安全架构、前沿安全技术、安全报告解读、安全趋势详解。做业内最专业、看透“安全本质”的纯安全分享公众号。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_642251d04229" alt="" />

---


### [君哥的体历](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MjQ1NTA4MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MjQ1NTA4MA==)

[:camera_flash:【2023-10-31 07:31:00】](https://mp.weixin.qq.com/s?__biz=MzI2MjQ1NTA4MA==&mid=2247490151&idx=1&sn=9272bfc80b93e0ce7ab39d731de5cf2b&chksm=ea4bb020dd3c3936f56612839626fb06d98d4ff40e8144555ff22f2a69b3ae011f2489654da4&scene=27#wechat_redirect)

闲暇时间，逼迫自己，记录分享体验与经历，不求正确统一，但求真、善、美。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_aa4d8db71365" alt="" />

---


### [商密君](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5NTM4OTQ5Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5NTM4OTQ5Mg==)

[:camera_flash:【2023-11-01 00:02:58】](https://mp.weixin.qq.com/s?__biz=MzI5NTM4OTQ5Mg==&mid=2247615190&idx=2&sn=19375bf46e5451576b0e6f18abd3d7c3&chksm=ec57128ddb209b9b322c41345ef88eebf93462f05207709cb85f89279e21575a18ef0b9e549d&scene=27#wechat_redirect)

商密君是全国密码行业头部公众号，旨在宣传推广《密码法》，普及商用密码知识，解读密码政策，分享商用密码行业的最新动态，为密码产业产学研用做好宣传服务，连接密码全行业精英，推动密码技术在数字经济各行业的应用和创新。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a02c9d0e1990" alt="" />

---


### [薄说安全](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NzU3MzkyMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NzU3MzkyMg==)

[:camera_flash:【2023-09-04 07:00:09】](https://mp.weixin.qq.com/s?__biz=Mzg2NzU3MzkyMg==&mid=2247484433&idx=1&sn=14bcc56b114028846335b17fecdc55ee&chksm=ceb8cddcf9cf44ca8d8b86621f0956a14f4406c5cca4a43175df48381c6ebb816a5632a1274a&scene=27#wechat_redirect)

关于功能安全、信息安全、系统工程方法的工作感悟，资讯分享。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_956e8befd9f9" alt="" />

---


### [数据安全架构与治理](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwOTc3NzMwMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwOTc3NzMwMQ==)

[:camera_flash:【2021-08-20 21:48:45】](https://mp.weixin.qq.com/s?__biz=MzAwOTc3NzMwMQ==&mid=2655246097&idx=1&sn=0036e0fa16fab9dd3eaf65eb2514b702&chksm=80ed0eebb79a87fd73221c49d1c59c9551a171f3feb5c2189b5bcefa5c11b44c4fb9a77796e5&scene=27#wechat_redirect)

数据安全架构设计、数据安全治理、隐私保护等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_beb639a4ea05" alt="" />

---


### [Sq1Map](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0NDg1MjQ0Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0NDg1MjQ0Nw==)

[:camera_flash:【2023-08-16 20:18:10】](https://mp.weixin.qq.com/s?__biz=MzU0NDg1MjQ0Nw==&mid=2247484501&idx=1&sn=32ac75ed3fd6940ce96bda55c9a5a8d0&chksm=fb7494e5cc031df3a5f54d63d3a7b82987312df4a91752fbb0340beca2ebd23bfef97805d373&scene=27#wechat_redirect)

甲方安全经验分享，你不再是一个人的安全部，我们一起组建组织，一起探讨学习 一起进步前行，个人博客: sq1map.com ，Github: https://github.com/jeansgit 欢迎留言评论提出宝贵意见建议，感谢！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_daa52e4d85e5" alt="" />

---


### [全栈网络空间安全](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NTUzOTg3NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NTUzOTg3NA==)

[:camera_flash:【2023-10-31 00:00:46】](https://mp.weixin.qq.com/s?__biz=Mzg3NTUzOTg3NA==&mid=2247509966&idx=1&sn=1a8612fe555c09661c93cdc30c1b8886&chksm=cf3d1b03f84a92153c43ae3c3252cd7a6aa51986f66ee35dcb6fd827e69df184c6e27d4b5201&scene=27#wechat_redirect)

做一个会思考的全栈网络空间安全者

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9774ce64d500" alt="" />

---


### [表图](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzOTI4NDQ3NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzOTI4NDQ3NA==)

[:camera_flash:【2023-10-08 12:10:41】](https://mp.weixin.qq.com/s?__biz=MzUzOTI4NDQ3NA==&mid=2247484501&idx=1&sn=03f2f85d5a109d3c180149073c8eedc3&chksm=facb82c0cdbc0bd6d6dabc2961cc919294219bdde7dc98f1c619495b4d304f378c832c07747a&scene=27#wechat_redirect)

坚信少却更好，坚定元认知传播，坚持安全美学，拿个主题，讲300秒就够了。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_717c729949b7" alt="" />

---


### [安全管理杂谈](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5MjEyOTE4MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5MjEyOTE4MA==)

[:camera_flash:【2023-07-12 18:00:15】](https://mp.weixin.qq.com/s?__biz=MzI5MjEyOTE4MA==&mid=2648519927&idx=1&sn=4ef0d92de9ffc6527f006a190e3d0bf5&chksm=f42f8404c3580d1209106774ab79dd2d7ec6760cb1f7043366005bd394c2d9699a87f4949db1&scene=27#wechat_redirect)

信息安全体系建设相关内容分享…不定时更新，可随时取关…

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_eaab7ac10752" alt="" />

---


### [掌控安全EDU](http://wechat.doonsec.com/wechat_echarts/?biz=MzUyODkwNDIyMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUyODkwNDIyMg==)

[:camera_flash:【2023-10-31 12:02:12】](https://mp.weixin.qq.com/s?__biz=MzUyODkwNDIyMg==&mid=2247532670&idx=2&sn=2c4045510c999fc76243b929f9e8f116&chksm=fa6b390fcd1cb01963f82a2521b7105d15ffd4abff009218223cd12f25e16225cdd4fa32776f&scene=27#wechat_redirect)

安全教程\\高质量文章\\面试经验分享，尽在#掌控安全EDU#

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_c028a5199606" alt="" />

---


### [数世咨询](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxNzA3MTgyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxNzA3MTgyNg==)

[:camera_flash:【2023-10-27 13:00:22】](https://mp.weixin.qq.com/s?__biz=MzkxNzA3MTgyNg==&mid=2247504282&idx=1&sn=faad6e4690473c7866bd271d9723e630&chksm=c144a327f6332a3111b6bc824e4d3eac06b1723e33094b32053f292a59143db3bdaced149a8a&scene=27#wechat_redirect)

中国数字产业领域中立的第三方调研机构，提供网络安全行业的调查、研究与咨询服务。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_bacd64503774" alt="" />

---


### [深圳市网络与信息安全行业协会](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0Mzk0NDQyOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0Mzk0NDQyOA==)

[:camera_flash:【2023-10-31 17:44:53】](https://mp.weixin.qq.com/s?__biz=MzU0Mzk0NDQyOA==&mid=2247511323&idx=1&sn=8cc8c78848d8f6bf5313d50b68e17ee4&chksm=fb015698cc76df8e6c4d1fe8032181f4b629abd3aec7c16aaf5d7e538c3a4be6496aaa191bd1&scene=27#wechat_redirect)

深圳市网络与信息安全行业协会（SNISA），是深圳市网络与信息安全行业的权威组织，是中国网络安全审查技术与认证中心（CCRC）在深圳唯一授权工作站，是经深圳市人力资源和社会保障局核准，开展网络空间安全工程技术专业职称评审工作的社会组织。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_650442d8cad4" alt="" />

---


### [放之](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3ODAzNjg5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3ODAzNjg5OA==)

[:camera_flash:【2023-08-17 08:53:56】](https://mp.weixin.qq.com/s?__biz=Mzg3ODAzNjg5OA==&mid=2247485228&idx=1&sn=84b5fdeae5362b21d0d248676b768bd6&chksm=cf1895e1f86f1cf761162c939141d405368c922b2283cff5e3e9756ff0a4c6bb273bdb70a263&scene=27#wechat_redirect)

持续关注企业与信息安全建设，分享精选干货。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4947400a6ef2" alt="" />

---


### [乐枕迭代日志](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NTMyNDg3OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NTMyNDg3OQ==)

[:camera_flash:【2023-10-24 15:13:58】](https://mp.weixin.qq.com/s?__biz=MzA3NTMyNDg3OQ==&mid=2652519629&idx=1&sn=3c288ffde05b1c9a4bae26fa22cc9954&chksm=849cd06db3eb597b8ff45387d761f6c68dca61247fa5f471e0c1de1be81d310896b8fcbb91f8&scene=27#wechat_redirect)

网络安全创业者，CTO@星阑科技。浪漫世界，一起折腾！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_6410a4e28c9e" alt="" />

---


### [青藤智库](http://wechat.doonsec.com/wechat_echarts/?biz=MzUyOTkwNTQ5Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUyOTkwNTQ5Mg==)

[:camera_flash:【2023-08-30 18:00:19】](https://mp.weixin.qq.com/s?__biz=MzUyOTkwNTQ5Mg==&mid=2247488291&idx=1&sn=1ca46da1bdce609737ff0b8ff24a28b2&chksm=fa58b118cd2f380e943741cc6d1472ae960346606aae7279324b696854db29a05a0010b1bebb&scene=0&xtrack=1&key=094fe642087a4fbec147824160a8f4a3b4e19435dd7adb86cda54974933694cff0b9911bd74d0431eedfd4092b38135bb4fa2f56535f3f16827d4f7dc50be33ca7e362d5f861f4adc98dfed962a06149d755b33dc6c5bf5d3b59fc804de03d60e9c8e1f423fcf75742ccb842162c3237721a380bfe199dd56f169c5b170d7483&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_7294abd08445&countrycode=AL&exportkey=n_ChQIAhIQo%2FfObtrzM1K3zI4tr0CPGBLuAQIE97dBBAEAAAAAAGllIeLgbRQAAAAOpnltbLcz9gKNyK89dVj0IN%2Bx5t6uM3YrBk5gKW%2Fp6afE%2BcmoIra%2BtExgE2Yk8JSZ2M0rYmheLIHj7vJyHgW%2FbQPt5Qr7kOUlHNe4yopZZR4FnrcZ%2FwimlSx7jszzD%2FDgJjGMJeVKI%2FeAkixMdw046GAUL0ht4e2GMd6Uac1nuGEPHe4gbLseLKiw4CfpdzPs%2FMu209wjp06YdINNMY%2BS1Uc4GLPp7FTytR9qg%2BvcM%2BWQMOvM20xwsCwey2Ji5zSZCtoFZEdG2ZocItp7RaOcxj73XxgB82Y%3D&acctmode=0&pass_ticket=%2BNgBFLt%2BEkvjU5aFwrbtU7OHWPCQOFuSjCaujZmNZX%2BdGl7kEYlDh5JT6D0Q93Zv&wx_header=0&fontgear=2&scene=27#wechat_redirect)

青藤智库，网安人的智囊团！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7294abd08445" alt="" />

---


### [白日放歌须纵9](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNjAyODE0NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNjAyODE0NQ==)

[:camera_flash:【2022-12-31 18:20:47】](https://mp.weixin.qq.com/s?__biz=MzIzNjAyODE0NQ==&mid=2247483931&idx=1&sn=cfd2112f1aae4ed538d6158911230142&chksm=e8df6b85dfa8e29334b9eefb98dac1377dd2bfdb3e07d55770624969e8ca4965b7afe94ddbc1&scene=27#wechat_redirect)

基于基本事实、基本常识和基本逻辑，致力大安全战略体系规划、数字化转型、以及产品管理方法论，提供领域的前沿理念和实践

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_119e00923f12" alt="" />

---


### [Fintech安全之路](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NjI1MzU4Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NjI1MzU4Mg==)

[:camera_flash:【2023-03-23 20:03:22】](https://mp.weixin.qq.com/s?__biz=Mzg3NjI1MzU4Mg==&mid=2247485009&idx=1&sn=43fe81ee814f41fedb98cbddeae4317c&chksm=cf345eb0f843d7a6efd3767e017c2405ac800880a399d5598915babbde0596d354692c661f1d&scene=27#wechat_redirect)

银行业应用安全的建设思考

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_763c23cd3870" alt="" />

---


### [网安网事](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5ODk0NTIxNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5ODk0NTIxNg==)

[:camera_flash:【2020-06-07 07:47:25】](https://mp.weixin.qq.com/s?__biz=MzU5ODk0NTIxNg==&mid=2247483929&idx=1&sn=42e4608d62b631ebd7b857c5d5b7bab9&chksm=febd3eb6c9cab7a063a35f4af64965998e7f83d6194860501ffabc387353425a6591c81578c1&scene=58&subscene=0&key=fb28ed52e6f6c17c0618b367296e77fa1ee2803d9cbf99af06b2d3ad06e5fc4bfa19acbb4beda00383c3b6cf2b52b0ef9714518d8994c8be8be66bd9f52556cca13aff96d1ab93f76defe860d13ed099a26792c94a83f9d368754af2c26837fbe56eac145c51dce009b1e8c70b80b8ef77a6f98013a80e9da3360b140642b89d&ascene=1&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=Az0EkeoSV7voXQ74ffgESy0%3D&a&scene=27#wechat_redirect)

随心所欲的树洞，网络安全，数据安全的随笔和心得。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_90820932a5f8" alt="" />

---


### [绿盟科技金融事业部](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2NDI5MTg4MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2NDI5MTg4MA==)

[:camera_flash:【2023-06-06 13:56:57】](https://mp.weixin.qq.com/s?__biz=MzI2NDI5MTg4MA==&mid=2247492964&idx=1&sn=eceee497a91fd6bee58185132d5f7f2a&chksm=eaac7623dddbff355cce21bedb9582253ed411a315f571c95280ddfb9e1a9466f2e2729409ef&scene=27#wechat_redirect)

该账号由绿盟科技金融事业部维护，用于发布金融行业相关安全资讯等内容

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_de59bd871eb8" alt="" />

---


### [vivo千镜](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0Njg4NzE3MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0Njg4NzE3MQ==)

[:camera_flash:【2023-10-19 12:00:18】](https://mp.weixin.qq.com/s?__biz=MzI0Njg4NzE3MQ==&mid=2247491244&idx=1&sn=a32ac32ae501bbfa886ea08bfa06ae6c&chksm=e9b938c0deceb1d64c07565a1c8351e21f441499bc9e1583543844ccc422b57f2f4cad7471b6&scene=27#wechat_redirect)

vivo全新的安全品牌，有人文温度的科技新媒体，致力于让亿万用户享受安全便捷的数字生活。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_54ff3f871510" alt="" />

---


### [网络安全观](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNzUxNzA2NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNzUxNzA2NQ==)

[:camera_flash:【2023-02-12 16:15:02】](https://mp.weixin.qq.com/s?__biz=MzIxNzUxNzA2NQ==&mid=2247495028&idx=1&sn=f37bc36bf24f0018c254b30dabd87cbd&chksm=97fa3012a08db90407cbbc81bff3bfcb8856a274b86d4bdda60dcedd3cfe6b7f6442aef03514&scene=27#wechat_redirect)

网络安全大脑与零信任传道者。睁眼看世界，抬头看战略，低头看产品。研究领域涉及XDR、零信任、身份安全、云安全、数据安全、美国与美军安全体系等。帐号主体为柯善学博士，现任职360。当前，本订阅号只做原创。谢绝商务合作与宣传推广。谢谢关注！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3290c66b9669" alt="" />

---


### [网络空间安全圈](http://wechat.doonsec.com/wechat_echarts/?biz=MzU1NDI2NDYxMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU1NDI2NDYxMg==)

[:camera_flash:【2021-07-18 18:01:13】](https://mp.weixin.qq.com/s?__biz=MzU1NDI2NDYxMg==&mid=2247483852&idx=1&sn=385b500994336ef721143a0727759d36&chksm=fbe77087cc90f991aac0fa02f7e01355526512dc056b2f3a10339971fb6de6888810434330cb&scene=27&key=12703ed085009c6ff980b759bbc11ff2198a1ee0c7b2f212eae47dc594b8ff15f8e3329303db060f471d94ac97440d0cf233f66ec264b5a896655bb2be97471585992540fa893d118fc32ece3a0ea98e602a24df0cfa5f0bab3b6016c3bbd565c6e363d2c884cf419b0e38511d8b6cf2ca173085f2ad02082faf6cfd42d5c736&ascene=0&uin=MTM1NzU2MDQ1OQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=AwFMaFDXwdoYkUhzPQry15Q%3D&acctmode=0&pass_ticket=lKHwS0Pg6YI1rR0qykEY71YGoYF92uoWu1UWkZ0miEdcXYymmsGIcYiFLGcG0J0%2B&wx_header=0&fontgear=2&scene=27#wechat_redirect)

网络空间安全领域毕业以及在读的博士们发起的网安观点与技术分享平台，皆在分享网安最新观点与技术，加强网安学术与产业交流。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4899831c31a0" alt="" />

---


### [FreeBuf安全咨询](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MTAwNzg1Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MTAwNzg1Ng==)

[:camera_flash:【2023-10-30 17:57:13】](https://mp.weixin.qq.com/s?__biz=Mzg2MTAwNzg1Ng==&mid=2247491428&idx=1&sn=199879a2a611daf3e398e97279c64107&chksm=ce1ce5fbf96b6ced8e0432ae9489cf5485d5778601e9bee03df3ada3af6906e45374c01795fb&scene=0&xtrack=1&key=16ff41fc38234ef8e67205d93b0d6e0f4d378c39024e95200d99e8f93644b95530df9383fc59279b5017c60f6a040c32825a0c11c1eaed3c4193b35dcfca2a6eab6e510b5939946ae80c330bd5c2bc024bf9124d60d3e5d4893233ec0b8b519a2ebb3fe6a0a31043786efa02ee0685ba2431501b5c29827595c6e99258209f84&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_e18f20900200&countrycode=AL&exportkey=n_ChQIAhIQrwggvN1UrefCuL6o72BiDxLuAQIE97dBBAEAAAAAABSPC1awC%2BcAAAAOpnltbLcz9gKNyK89dVj0EEKqhSFRQAt1Y2oQLjKk%2Fu1jLrR4EU8sgxaPXlxA2cdsms6iXGoloFMbb1mqdQrBhIHY14NDnwHkzAlP1rNs%2BigSUQNj8IlCr%2BxJs5Ps5kVdppmNG1OX8bEG%2BR4sLF%2BG%2F7MQ9XJyMzJRx%2B%2FB4%2BcBY8kN1u4louWnvBMZEV0TqVsLMYBDlLlv4s3XrO%2F4RoM9DPkCJvR2zXkQgzBtkgyJ17It%2FenKsh5rE8uqPPsg%2FeOaD3rMPyjw9e1N%2FOIsvEMx7xAf22HeX5o%3D&acctmode=0&pass_ticket=ydhFrcEv6PrO3uwrW%2BXF75oEG8Fve6mk8x6AJZoOn9okJt2F4nr%2F2EYz2m7RVuQ6&wx_header=0&fontgear=2&scene=27#wechat_redirect)

关注安全行业前沿动向、产品服务商用前景，洞悉商业市场和用户趋势，助力安全行业发展。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e18f20900200" alt="" />

---


### [悬镜安全说](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwODc2NjgwMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwODc2NjgwMg==)

[:camera_flash:【2023-09-08 09:29:49】](https://mp.weixin.qq.com/s?__biz=MzAwODc2NjgwMg==&mid=2649111703&idx=1&sn=0f5c84a8628bba593ef5159c8ac33610&chksm=837b48aab40cc1bc7139b5c39ef6d36c0bee3f9be55d6f00adc06ecb9c162dfce35edfb45d37&scene=27#wechat_redirect)

悬镜安全，DevSecOps敏捷安全领导者，创始人子芽。专注于以代码疫苗技术为内核，通过原创专利级第三代DevSecOps智适应威胁管理体系，持续赋能数千家行业标杆用户，帮助其构筑起内生积极防御体系。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7dd1a6c278da" alt="" />

---


### [信息安全运营](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2ODk1MTM5NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2ODk1MTM5NA==)

[:camera_flash:【2023-02-13 07:05:15】](https://mp.weixin.qq.com/s?__biz=MzU2ODk1MTM5NA==&mid=2247485047&idx=1&sn=918ae3ac50885010c431f1723f09f474&chksm=fc87517acbf0d86cc1c05e2dd86a3fa60a1e84fae0ee44dd2fb3b7a190de1c179357baa8845b&scene=27#wechat_redirect)

信息安全运营原创实践分享等

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_66b42f2c03d3" alt="" />

---


### [阿肯的不惑之年](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMjUzNzYyNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMjUzNzYyNA==)

[:camera_flash:【2023-08-25 15:08:52】](https://mp.weixin.qq.com/s?__biz=MzIyMjUzNzYyNA==&mid=2247484363&idx=1&sn=920de5d4dc626e2103a01b839db93d9c&chksm=e82ab13cdf5d382a57084cb6afdcefb9c0d68c72b30373d2cb04ccbd55b2843d338629d1ced0&scene=27#wechat_redirect)

生活工作上努力成为最好自己的各类感想；企业信息安全建设的趋势和方向随笔。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ff60192713b6" alt="" />

---


### [SecOps急行军](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Mjc5MDQ3NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Mjc5MDQ3NA==)

[:camera_flash:【2023-06-12 09:00:03】](https://mp.weixin.qq.com/s?__biz=MjM5Mjc5MDQ3NA==&mid=2652056308&idx=1&sn=163b38f9f93cad6761d49664d90cdd02&chksm=bd4768518a30e147cef83675dfc1001c722c4fabc361bbb2488b5fac230c618c90f0230602e4&scene=27#wechat_redirect)

安全和运维不分家，要两手抓，两手硬。Security：安全建设哪有什么圣杯，无非是日拱一卒的心态和对解决问题的执拗。Operation：只要不断地解决根源问题，服务质量就一定会得到提升。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_390493bef72f" alt="" />

---


### [腾讯安全战略研究](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5MTA0NjU3Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5MTA0NjU3Ng==)

[:camera_flash:【2023-10-17 16:19:54】](https://mp.weixin.qq.com/s?__biz=MjM5MTA0NjU3Ng==&mid=2652720013&idx=1&sn=3f8f6921bad2cfb70728106827863751&chksm=bd5265528a25ec44096b8f5214021f5a74caf71b6580da5bf14913635281b9476383c158ede0&scene=27#wechat_redirect)

聚焦网络安全法律研究，剖析网络犯罪现状手法，洞察网络传播规律特征，问道数字空间生态治理。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_6e302b961ec6" alt="" />

---


### [冷眼安全观](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MDI5MTQzMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MDI5MTQzMg==)

[:camera_flash:【2022-09-16 14:21:24】](https://mp.weixin.qq.com/s?__biz=Mzk0MDI5MTQzMg==&mid=2247483919&idx=1&sn=414d3604da010ae2d073fd1102c04d28&chksm=c2e2a608f5952f1ee2a783e880ff027612febf89722d4450a646367542e062cdd2ab4f2c1f0e&scene=27#wechat_redirect)

网络与信息安全世界纷纷攘攘，利益与技术纠缠交织，陷阱重重。希望在混沌的时代，从中立的角度思考安全最本真的那条路，梳理网络安全的过去、现在与未来，在迷雾的街角点亮一盏灯。版主：周智，wx：zhouzhi20140516

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_66aad224f32e" alt="" />

---

