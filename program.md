
### [进击的Coder](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNzA4NDk3Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNzA4NDk3Nw==)

[:camera_flash:【2021-02-23 20:30:00】](https://mp.weixin.qq.com/s?__biz=MzIzNzA4NDk3Nw==&mid=2457749656&idx=1&sn=211ec38a68c1defea6eaba829ec38c23&chksm=ff44e0c6c83369d00fedb2d792a9dcdd52c9a106452fb5b629b78cab75b54ddeac3f70f9b6dc&scene=27#wechat_redirect)

分享技术文章和编程经验，内容多为网络爬虫、机器学习、Web 开发等方向。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5b0546ddd2d0" alt="" />

---


### [数据库开发](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3NDA4OTk1OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3NDA4OTk1OQ==)

[:camera_flash:【2022-05-22 12:23:20】](https://mp.weixin.qq.com/s?__biz=MzI3NDA4OTk1OQ==&mid=2649918481&idx=2&sn=b472d76c53385e4c98940c26c4a4f504&chksm=f31f7e99c468f78ff8c42c444ebff17a0a821843ecd4be3650063fda7e643cb15205a04c4cb1&scene=27#wechat_redirect)

分享数据库开发、原理和应用，涵盖MySQL、PostgreSQL、MS SQL Sever、Oracle等主流关系数据库的应用和原理，以及MongoDB、Redis、Memcached等NoSQL数据库和缓存技术。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7aa87fe670ff" alt="" />

---


### [进击的Hunter](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzAxNzYxMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzAxNzYxMQ==)

[:camera_flash:【2021-01-15 20:11:20】](https://mp.weixin.qq.com/s?__biz=Mzg4MzAxNzYxMQ==&mid=2247484270&idx=1&sn=1eaf01665f99b3f2ece344a3adff986c&chksm=cf4c918af83b189cf5eee9db49ba753dc25c041d82e4fa685a22e07033456917304157524686&scene=27#wechat_redirect)

专注Python、爬虫、后端、Poc、Exp及各种骚操作。各位如有什么想法，请私信我~

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3093cf54140f" alt="" />

---


### [安全学术圈](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5MTM5MTQ2MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5MTM5MTQ2MA==)

[:camera_flash:【2023-10-30 23:26:32】](https://mp.weixin.qq.com/s?__biz=MzU5MTM5MTQ2MA==&mid=2247489943&idx=1&sn=6d1d0f4101d7216403ba1151efe0c557&chksm=fe2ee61cc9596f0a4c08e00217a0c01ba3fd8727cd21841da62dae012844a30bc2c877501977&scene=27#wechat_redirect)

分享安全方面的论文写作、会议发表、基金申请方面的资料。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_cbda1fb027bf" alt="" />

---


### [SIGAI](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4MjQ3MDkwNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4MjQ3MDkwNA==)

[:camera_flash:【2023-10-16 09:49:19】](https://mp.weixin.qq.com/s?__biz=MzU4MjQ3MDkwNA==&mid=2247492099&idx=1&sn=daad804b698f9e80f1302b646cdc3a19&chksm=fdb57f94cac2f68255c5e3866f0c3cd89388e63d74888b7ee497cea63b42e13d783d5e2c8f80&scene=27#wechat_redirect)

全方位覆盖AI经典算法与工业应用，紧跟业界新趋势，让你始终站在技术前沿。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0339db23ffb8" alt="" />

---


### [NightTeam](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5NzIyMzkzNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5NzIyMzkzNw==)

[:camera_flash:【2022-08-29 10:27:06】](https://mp.weixin.qq.com/s?__biz=Mzg5NzIyMzkzNw==&mid=2247486118&idx=1&sn=ca13642eb51977a63e42a9eb8809aa67&chksm=c07456c8f703dfde0e5f3d3490c59382a457d36a617ada7c6426f76400912150ae45b6349795&scene=27#wechat_redirect)

夜幕团队公众号，涉猎的编程语言包括 Python、Rust、C++、Go，领域涵盖爬虫、深度学习、服务研发和对象存储等，反正谁先关注，谁先涨工资。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b46c4f4dc23b" alt="" />

---


### [七夜安全博客](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwODIxMjc4MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwODIxMjc4MQ==)

[:camera_flash:【2023-09-21 11:35:17】](https://mp.weixin.qq.com/s?__biz=MzIwODIxMjc4MQ==&mid=2651004951&idx=1&sn=6447e68a4d5868740553349fdec20f9f&chksm=8cf10655bb868f430ad6aae5f5bdfb0b1e374229c2517549d75ee8e3d124086effab204a1083&scene=27#wechat_redirect)

和七夜一起去探索人生的星辰大海，技术人并不只有技术，你要的人生成长与自由在这里

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_20344080d59d" alt="" />

---


### [美团技术团队](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5NjQ5MTI5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5NjQ5MTI5OA==)

[:camera_flash:【2023-10-26 19:59:00】](https://mp.weixin.qq.com/s?__biz=MjM5NjQ5MTI5OA==&mid=2651775540&idx=1&sn=9b6f3a0e1d3c03f6c14cf3f0c146f7f3&chksm=bd123b798a65b26fe61dab5cdd096a4d0121eb0a02364c92373da092ef75e8a8c55f0c146138&scene=0&xtrack=1&key=79faf193ca39ac843467e58fed14b5508a5672bdf62fe1abb3b013cf50081ffea245996d84281ec7e4999c488cdc725b6459e2dc971303b1a83d3b691530f42728aaddb8c08173ebc940f1b475818e8c925967069cd54cdfe2ca93d3e1aa75deda040fb38f695559efa53464425b01f4e9c14fd8ef4c41e0234a4c5000e8d065&ascene=51&uin=MzgxODQ4MjMz&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_96d37a04e9bf&countrycode=GY&exportkey=n_ChQIAhIQgUGp4kQsfQabvjjZj1GaTRLvAQIE97dBBAEAAAAAAL4eJHzWqtUAAAAOpnltbLcz9gKNyK89dVj0Kubsvh0gcwiDy6uNrQGZDeclkemRpOqdv8HmXbogrojKNX76wBGbT4%2BGk78Cwn%2FBTr%2Bb%2F9gwwrmRhyPqaBSCjn2dMsR7E4HcUpwOXyF8DDposAdgldT0wElH15TZI%2F8pxtsQwZHwrpJUpBuz0TPaCpVvQS3IRkOGIrIW5UMpfknKqIEAKc4jXsj6eHU6uUmdgBcnbSui3i7rYCiMhNiSafkhQ%2BLJdBQe7f%2FIDkG02Hc6H4H84XSWW0H9IGMCdbzpH%2F9Id2h%2Bhz1n&acctmode=0&pass_ticket=w8fJ8bBY1Hlkz0lFunaePrp2VkYDNdEqAiQi%2BSpnxQ9TAzcG62J00XC%2FAlJKhsTr&wx_header=0&fontgear=2&scene=27#wechat_redirect)

10000+工程师，如何支撑中国领先的生活服务电子商务平台？数亿消费者、数百万商户、2000多个行业背后是哪些技术在支撑？这里是美团、大众点评、美团外卖、美团优选等技术团队对外交流的窗口。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_96d37a04e9bf" alt="" />

---


### [爱奇艺技术产品团队](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0MjczMjM2NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0MjczMjM2NA==)

[:camera_flash:【2023-10-24 18:37:02】](https://mp.weixin.qq.com/s?__biz=MzI0MjczMjM2NA==&mid=2247497625&idx=1&sn=8d0a211519922776e18bd9ab756ab35e&chksm=e9756bbade02e2ac17e3cc8450ff02122c44aa29282ed68fba7742feec3491769f252daa082e&scene=0&xtrack=1&key=b6e74e278693a9f3069d9c01c6ef27ef6c506f77d360489ba02f361ea3db7738c5e291f59519c376975142345d48e20e93fd0e90706f6417a25826bd628d5e7f550c08df30f83688a835eefdd96239a17ac5386e918226819728b93fdbcc4736c23d6c962c76d2b616e694bdd8113cbe13be6e6c51f55fe9ef8c0249f8c106c1&ascene=51&uin=MzgxODQ4MjMz&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_aa1476c2ce41&countrycode=GY&exportkey=n_ChQIAhIQS%2F6O9xzLPnfd6ASiSKB%2FhRLvAQIE97dBBAEAAAAAABqoAIB2Aw8AAAAOpnltbLcz9gKNyK89dVj0Er5SCd4Op6Npn%2FhqaSOV%2FOb%2B%2F4SogbOdXO7yNjaAzWQdWFj7ksHYfE08zGyYeI1kNLqGpunXnMS88BGaDYUUvsuLA7H7mv7XLfruQXZtIoZYplAByuwehfTEoQTWw9RqJohGjVUVbegrVyNh4NOxEMg1KlTjHUf5bO0LFdJD8EcrfJ8BvEBPqTraET8VwiHjwouHFNaScoFUlmqmO9X5GlGfuiUIyW7iZ27HJM%2F7Sr395MwCQ3YxPP2NAX6EYMBilfi%2BI9xyHW4e&acctmode=0&pass_ticket=HM%2B1G%2FWsriaEFiWV03p2dP0kM0B86gEXS5LT8AmZwMl0jVuSgv6%2B4dP85V7YzP98&wx_header=0&fontgear=2&scene=27#wechat_redirect)

爱奇艺的技术产品团队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_aa1476c2ce41" alt="" />

---


### [GoCN](http://wechat.doonsec.com/wechat_echarts/?biz=MzA4ODg0NDkzOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA4ODg0NDkzOA==)

[:camera_flash:【2023-08-04 08:00:40】](https://mp.weixin.qq.com/s?__biz=MzA4ODg0NDkzOA==&mid=2247508397&idx=1&sn=b9f0bf7b1bfffb1a165bb54d4b0ad554&chksm=90211e48a756975efbf4c424359af2862fd2524e255394d98b6773f8504db82e08374d071021&scene=27#wechat_redirect)

最具规模和生命力的 Go 开发者社区

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_8997cd77cfb2" alt="" />

---


### [腾讯安全智能](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2NDUyMjAyOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2NDUyMjAyOA==)

[:camera_flash:【2021-11-29 12:05:00】](https://mp.weixin.qq.com/s?__biz=MzI2NDUyMjAyOA==&mid=2247488148&idx=1&sn=ba989e532922cc893b76748ddd35eca7&chksm=eaaa02d7dddd8bc111db54bddc5708ff7986be66cf9559d11bf616d560549b9de59aa6f79781&scene=27#wechat_redirect)

该账号主要围绕智能化技术如何帮助企业提升网络安全水平展开，内容涉及机器学习、大数据处理等智能化技术在安全领域的实践经验分享，业界领先的产品和前沿趋势的解读分析等。通过分享、交流，推动安全智能的落地、应用。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_dc9b9a9fb2b2" alt="" />

---


### [字节跳动技术团队](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MzYzMjE0MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MzYzMjE0MQ==)

[:camera_flash:【2023-10-30 10:39:27】](https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247504566&idx=1&sn=64b198119d5782daba63490d8580b368&chksm=e9d31954dea49042ee7bee78df90d49181a986b93d06fbb5093ea3a17f545bd35d288e197dc5&scene=27#wechat_redirect)

字节跳动的技术实践分享

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_24231986c9c8" alt="" />

---


### [GobySec](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4MzcwNTAzOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4MzcwNTAzOQ==)

[:camera_flash:【2023-10-26 11:52:29】](https://mp.weixin.qq.com/s?__biz=MzI4MzcwNTAzOQ==&mid=2247535253&idx=1&sn=a63da635da47e36d7a285a69fff6f365&chksm=eb84a135dcf32823c3baa9ffe8764369b8ed1dbda68729cb451512fb86f09740320c78689cd8&scene=27#wechat_redirect)

新一代网络安全测试工具，由赵武Zwell（Pangolin、FOFA作者）打造，能够针对一个目标企业梳理最全的攻击面信息，同时能进行高效、实战化漏洞扫描，并快速的从一个验证入口点，切换到横向。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7f7c15f37480" alt="" />

---


### [小生观察室](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxODQ1OTg5NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxODQ1OTg5NQ==)

[:camera_flash:【2023-04-01 08:00:08】](https://mp.weixin.qq.com/s?__biz=MzIxODQ1OTg5NQ==&mid=2247485334&idx=1&sn=15ba8f9b904484ed3d75640db1b11956&chksm=97eb7c0ea09cf5186ec8a427eff1fbcc2f4498c0fc75f67c565462da646f8281124489416e49&scene=27#wechat_redirect)

本观察室仅个人做内容存档使用！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_29a59093e5db" alt="" />

---


### [安全研发](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Nzk3NTIwOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Nzk3NTIwOA==)

[:camera_flash:【2021-07-31 22:32:10】](https://mp.weixin.qq.com/s?__biz=MjM5Nzk3NTIwOA==&mid=2247484987&idx=1&sn=9c82335297de9ffcc948f4168c4f3a54&chksm=a6d083fc91a70aea7b12f0142e63efcf51faa58a5c09614dce9d632c5b6b7133faf577848441&scene=27#wechat_redirect)

专注于分享Python-安全研发课程-漏洞扫描器-爬虫-自动化工具-网络安全等相关资料

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a1b9a964503c" alt="" />

---


### [huasec](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyOTY1NDE5Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyOTY1NDE5Mg==)

[:camera_flash:【2023-08-27 18:38:19】](https://mp.weixin.qq.com/s?__biz=MzIyOTY1NDE5Mg==&mid=2247485088&idx=1&sn=41b25c7ac567da746d6ee258e7c41e46&chksm=e8be2d7fdfc9a4696d1921a328978f0110655db8dde3a82f332bf95a4d4f88e7f12947932f49&scene=27#wechat_redirect)

分享一些平时所学，励志成为一名安全研发。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d9544647f40b" alt="" />

---


### [街头香蕉](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMDQyNDcyOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMDQyNDcyOA==)

[:camera_flash:【2023-03-23 17:37:46】](https://mp.weixin.qq.com/s?__biz=MzIyMDQyNDcyOA==&mid=2247483801&idx=1&sn=dfdd988d4af253696dca17ff1e464a3b&chksm=97cd7eeea0baf7f80b5b20f87540b9aebe7dbe150c597d43daecb96fcbf573afa6b27e8d1582&scene=27#wechat_redirect)

街头香蕉

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_19b5fbaa9c06" alt="" />

---


### [读诗库](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5NzQ3NzIwMA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5NzQ3NzIwMA==)

[:camera_flash:【2023-10-23 20:10:37】](https://mp.weixin.qq.com/s?__biz=MzU5NzQ3NzIwMA==&mid=2247483927&idx=1&sn=30c14953dac950322ad73de95544450f&chksm=fe539028c924193e6b65190ca677caf697de782276825d315eb3029a419886cf8ec881767d2f&scene=27#wechat_redirect)

读书，写诗，假装自己是黑客。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f524e5a0a1fb" alt="" />

---


### [编码安全研究](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NDY1MDc2Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NDY1MDc2Mg==)

[:camera_flash:【2023-10-26 10:01:40】](https://mp.weixin.qq.com/s?__biz=Mzg2NDY1MDc2Mg==&mid=2247504093&idx=2&sn=9ecd6660bc017233219a9556edb523dc&chksm=ce6483b8f9130aaeccbc6270ceb686dd9da7ba4d4dc17a684477fbe8eabf732a1874620f30d0&scene=27#wechat_redirect)

专注于学习网络安全

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_8e01ffd3a47c" alt="" />

---


### [全闲话](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMTkxMjMwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMTkxMjMwNg==)

[:camera_flash:【2023-03-24 16:30:56】](https://mp.weixin.qq.com/s?__biz=MzUzMTkxMjMwNg==&mid=2247484086&idx=1&sn=8bb4431861a5915b727ebc9658eea5a1&chksm=faba01abcdcd88bd766e277f70c6050ea0e2c4f542636367db5feff7a333a73048373b602a1d&scene=27#wechat_redirect)

这里只有闲话和大闲话，据科学家团队开发的人工智障严重分析得出结论：此号会聚焦安全和运营领域并且一本正经那是不可能的！请谨慎关注~

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0b7d3b5806ab" alt="" />

---


### [Yaklang](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxOTAzOTU3Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxOTAzOTU3Mw==)

[:camera_flash:【2022-07-01 17:09:01】](https://mp.weixin.qq.com/s?__biz=MzAxOTAzOTU3Mw==&mid=2247488757&idx=1&sn=eaeb8485a63ae1626ca071db52ab5eda&chksm=9bcc40cfacbbc9d964761c7bbb41942eea0d636b92e7c7afef501d781e3334980f8746f8da95&scene=27#wechat_redirect)

Yak Language Project: &lt;del&gt;北半球&lt;/del&gt;最强安全研发语言 / 黑客语言

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f52cadd4bf58" alt="" />

---


### [360Quake空间测绘](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NzE4MDE2NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NzE4MDE2NA==)

[:camera_flash:【2023-09-27 11:04:45】](https://mp.weixin.qq.com/s?__biz=Mzk0NzE4MDE2NA==&mid=2247487746&idx=1&sn=27a09397a248ccb28ed1f8872f98a41f&chksm=c37b96e9f40c1fff9efbdb2ff5f83645a3e7be12ba8467dfc20501588ecf1cfff150515d58ee&scene=27#wechat_redirect)

360 网络空间测绘系统（QUAKE) 是 360 网络安全响应中心（360-CERT）自主设计研发的全球网络空间测绘系统，能够对全球 IPv4、IPv6 地址进行持续性探测，实时感知全球网络空间中各类资产并发现其安全风险。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0284fd4851e7" alt="" />

---


### [RapidDNS](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NDU0ODMxOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NDU0ODMxOQ==)

[:camera_flash:【2023-07-10 08:45:47】](https://mp.weixin.qq.com/s?__biz=Mzg4NDU0ODMxOQ==&mid=2247485785&idx=1&sn=404692ca0db67c74399f763954eadba7&chksm=cfb73ee9f8c0b7ff6b433386816f558b8c60b54561d8b2ee3554719990b1d69ef8789b7b2204&scene=27#wechat_redirect)

RapidDNS.io 是一个免费开放的DNS在线查询平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_6327c9075859" alt="" />

---


### [腾讯代码安全检查Xcheck](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2ODQ3ODE1NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2ODQ3ODE1NA==)

[:camera_flash:【2023-05-23 16:01:46】](https://mp.weixin.qq.com/s?__biz=Mzg2ODQ3ODE1NA==&mid=2247486394&idx=1&sn=08687b34c05af8c132bd39e06e80e4a1&chksm=ceaaf61ff9dd7f0933bb44d5616e0ff1cf66b41361e39ab2e4f21836461b7b0dd384dc74f768&scene=27#wechat_redirect)

腾讯代码安全检查Xcheck，是一个腾讯公司自研的静态应用安全测试(SAST，Static application security testing)工具，致力于挖掘代码中隐藏的安全风险，提升代码安全质量。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_177b81103e8d" alt="" />

---


### [灾难控制局](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NTc1NTcwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NTc1NTcwNg==)

[:camera_flash:【2023-09-19 10:27:26】](https://mp.weixin.qq.com/s?__biz=MzI1NTc1NTcwNg==&mid=2247484378&idx=1&sn=0180872c818fef3e2b0bb7917bbf82e6&chksm=ea30570fdd47de194f9b1e026fce6c4bec03085cd68944c8610e13c672eb7106898406632718&scene=27#wechat_redirect)

Will的小屋

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_2c6f783d354d" alt="" />

---


### [腾讯技术工程](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5ODYwMjI2MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5ODYwMjI2MA==)

[:camera_flash:【2023-10-31 18:01:01】](https://mp.weixin.qq.com/s?__biz=MjM5ODYwMjI2MA==&mid=2649781000&idx=2&sn=3e59cbbdcbe70b76f816b726c4492ecc&chksm=becce07389bb6965af9de719947b8cca30e69b1e4f837caa8873c45971c924a512d446d98944&scene=27#wechat_redirect)

腾讯技术官方号。腾讯技术创新、前沿领域发布解读平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d14465b5ce6c" alt="" />

---


### [哔哩哔哩技术](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3Njc0NTgwMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3Njc0NTgwMg==)

[:camera_flash:【2023-10-31 12:02:18】](https://mp.weixin.qq.com/s?__biz=Mzg3Njc0NTgwMg==&mid=2247497146&idx=1&sn=e1bbc0e506fa4d625eeb469dfa794fca&chksm=cf2f309ff858b989fcfffea817a52603b35846de0a748a676654fc55cfdd86ec4adf61fe7491&scene=27#wechat_redirect)

提供B站相关技术的介绍和讲解

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_6a92a838f9e0" alt="" />

---


### [Pinpoint代码审计](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwOTIyMTE1Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwOTIyMTE1Ng==)

[:camera_flash:【2022-04-02 10:00:28】](https://mp.weixin.qq.com/s?__biz=MzkwOTIyMTE1Ng==&mid=2247483847&idx=1&sn=60fe8835b980d3d0c23304e1f9627b0f&chksm=c13f4d8cf648c49a495fc6f5bfc52a05f09f5d0f2071c88b21dcfe6f5afd1e19840790121984&scene=27&key=da5527f6ccd6edd78869364ca0cd12906cd535b106933fbc44a52aa82163f675dc3e8e542a623f45bc31ec61b07b5c5ea90d0732939416b0da7e7df4ec2034f37257aa16361813ff7c7d5c8e07309913db43be3cf29483a812d676a96ba89b0a5b7053948e17227b7662e21da4f942a3eb45b521aff6915902a2ef9c72847c98&ascene=0&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=AaNGCbad90YXW9PQVzqBDtY%3D&acctmode=0&pass_ticket=ZxdeshBmtG8rj%2Fsaib1FFmq9XdxP45CJlTqYSx7i3x24MF8aRwaQsSVQrT%2BCFvNS&wx_header=0&fontgear=2&scene=27#wechat_redirect)

Pinpoint，专业代码审计团队。感谢您的关注！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f580fec03d4d" alt="" />

---


### [逆向lin狗](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMjU3ODc1MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMjU3ODc1MA==)

[:camera_flash:【2023-07-05 11:51:59】](https://mp.weixin.qq.com/s?__biz=MzUxMjU3ODc1MA==&mid=2247485894&idx=1&sn=09b7c8a1ebcb949864416eb1b1cdec3d&chksm=f9630c17ce148501ea7f4d436033b3e90c211b2bda7c3a1f8366b40d9322dd8b953b8d8bb43d&scene=27#wechat_redirect)

本公众号由善于Web反反爬的林弟弟善于App逆向的猛狗哥哥赞助

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_fc6c78a6258d" alt="" />

---


### [逆向OneByOne](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5NTcyMDc1Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5NTcyMDc1Ng==)

[:camera_flash:【2023-10-17 09:15:06】](https://mp.weixin.qq.com/s?__biz=MzU5NTcyMDc1Ng==&mid=2247490767&idx=1&sn=277f678f31b6ea0bdfee065398678e27&chksm=fe6cf394c91b7a82da3445aa6715b314d068cc354aea43cc6646ed9bf5b3c7f9bfa98b42d557&scene=27#wechat_redirect)

逆向案例小笔记

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_84c656527267" alt="" />

---


### [QingScan](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5NzEwOTUzMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5NzEwOTUzMw==)

[:camera_flash:【2023-10-31 17:49:23】](https://mp.weixin.qq.com/s?__biz=MzA5NzEwOTUzMw==&mid=2447979944&idx=1&sn=c2e06b544a43fa1a0222178d377819ab&chksm=84b846dab3cfcfcc93bd15b3d2b5279f153e2b2dba98699dbd20d660e2cdca6f08a542c00465&scene=27#wechat_redirect)

关注我，及时收取你遇到的问题

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_c29b8eca5b6e" alt="" />

---


### [FOFA](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzIwMzY4OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzIwMzY4OQ==)

[:camera_flash:【2023-08-17 16:16:26】](https://mp.weixin.qq.com/s?__biz=MzkyNzIwMzY4OQ==&mid=2247488795&idx=1&sn=78191eec5247075aa8d5b54784078022&chksm=c22afd1df55d740b9250774074121e7d39e9dc9ed73f8dc7c7dbea7ed3b9f20fe02a4dd6e33f&scene=126&sessionid=1693373763&subscene=227&clicktime=1693373764&enterid=1693373764&key=42e64e73470f56adbe6aa8e71422b5cf99bf1f3061199d54f0501073d06d8f0de40f0611c5fab9848221ee29ef29d73853ca02e2e7143585db4f6f24eaa7446221a13b7b0427d6972089400aca73e1ce2840eaea0e6734ae48c46effe64ef212a3c6853cae2ecb1f8e21fb792246874676d05a8485defd3f360033184c06944d&ascene=7&uin=MTI5ODM0MTMwNQ%3D%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&exportkey=n_ChQIAhIQhRzgFuj6IG2rKlzO4k68txLgAQIE97dBBAEAAAAAAOMjNL2v0vwAAAAOpnltbLcz9gKNyK89dVj0ZKh6AQFWfJwkx%2FuPdRjs0wrBvU2asoFoxPBAlEiBzvbAq98mVEZtBTzGenNlhSCMC9kyADy2CAgmmut8wjJlueulzGa31Mg2hpTmQpTOawHZjMqvOGOH8vr%2BHk2aFIKa3JZMnQtL8E0DByrnLf9Oml%2FTRTIZVsihkT%2BmgM56NTAt%2FNRbhyHl028vcmDfSXy0ca3v4atmjQ6Wxl%2F2OmiFIUJt2GhEGxIY6uMh%2BOd41RH3nBblrk5WoszQ&acctmode=0&pass_ticket=NfjiuSXvmKFYTN92KjBS5l74bwjloNo0lDchQpMi7IFSB5v3lg6IY7PJrd9UvoY8&wx_header=0&fontgear=2&scene=27#wechat_redirect)

FOFA是由华顺信安科技有限公司开发的一款网络空间资产搜索引擎。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d79c8913fde8" alt="" />

---


### [HaoDF技术团队](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxOTg4NDAxMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxOTg4NDAxMg==)

[:camera_flash:【2023-02-02 18:27:35】](https://mp.weixin.qq.com/s?__biz=MzUxOTg4NDAxMg==&mid=2247484375&idx=1&sn=c3cd2fb73806b8d94f79cd8b17836b6c&chksm=f9f39e54ce84174289350b9f558b3ce0b9710d52b31d11e2d782f1e030684e0c35208b00b252&scene=27#wechat_redirect)

好大夫在线技术实践与分享，欢迎大家一起交流！更欢迎加入我们，一起用“科技创造优质医疗”！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a0f9bc95946e" alt="" />

---


### [安第斯智能云](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzE2MzY1OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzE2MzY1OA==)

[:camera_flash:【2023-10-30 21:14:33】](https://mp.weixin.qq.com/s?__biz=Mzg4MzE2MzY1OA==&mid=2247497460&idx=1&sn=e47cee32370b4d5efb2e6148289b097d&chksm=cf492191f83ea88716ac5135befd2f902efbf7085aa661525be7cfed323bb967ad6bdc55ffb8&scene=27#wechat_redirect)

OPPO 安第斯智能云（AndesBrain）是服务个人、家庭与开发者的泛终端智能云，致力于“让终端更智能”。作为 OPPO 三大核心技术之一，安第斯智能云提供端云协同的数据存储与智能计算服务，是万物互融的“数智大脑”。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7bc48466f080" alt="" />

---


### [北邮GAMMALab](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzE1MTQzNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MzE1MTQzNw==)

[:camera_flash:【2023-10-27 11:12:09】](https://mp.weixin.qq.com/s?__biz=Mzg4MzE1MTQzNw==&mid=2247488425&idx=1&sn=4a18352cdbb51c76af284ee839f4241d&chksm=cf4a948bf83d1d9d5975ddfe280ee04ffd13ecfea56bcb3918a581cf9cd896902a230763b0ab&scene=27#wechat_redirect)

北邮图数据挖掘与机器学习实验室

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_bc48147b67b6" alt="" />

---


### [隐查查](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwODMxMjcyMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwODMxMjcyMQ==)

[:camera_flash:【2023-10-23 10:22:00】](https://mp.weixin.qq.com/s?__biz=MzkwODMxMjcyMQ==&mid=2247487101&idx=1&sn=4a81f5c76f9e992d6abff6a872d5e89f&chksm=c0caa783f7bd2e9573fb161f475e87b59478f1b6f5a7703150d66e9d320634d547e962e57bb0&scene=0&xtrack=1&key=d842c29fb9ae087ab72761a2de4fe0eca1a2ac66fd518739ce09d332cdaf7507ce11cd8f9adc41539462e12d7d38f029323d37a87fd15e2c349107726a42d75358ff51780a9dd402ae50d384b96386d95abdce7a19c224e7537e435c7e9904d16a10f111e195e01f35b29353865b4faf5ed72e0c2ed4cd82801d9984674cfa17&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_9701d03894ec&countrycode=AL&exportkey=n_ChQIAhIQJmnioKJPCd98DLE9B9ZHjxLuAQIE97dBBAEAAAAAAK1TCJmb47sAAAAOpnltbLcz9gKNyK89dVj0C%2Fezf0BYooCIcj%2FukcwH7M767eTjYg%2FdpoI6rTvJkCKGnWkOcueZyWrUxecOEkVcJQPeqZDnI%2B%2FtcK8vX5YnAMvhUX1T5o0XkpVdZmofM%2Fr3gSjJZQ0Rnh03NswDvpe6dd3dFFvJoFksumLtFHo7zeu45SjcBCFZPzNkUnb4GuMYMAfb%2FXljep8YcWMZU5MYmFiQ5V5Y2FYb4ir71pQ1j2zYB3PtTWZwpz0itpvhja%2FO7xS%2FGNdg5H7lY9frRnXLS68s1YrAi3w%3D&acctmode=0&pass_ticket=rnFECTIUoOr9%2Bss78n3h%2BKEOYtNOuZgJoW1oGvTj8uwPq9FghqJs6S2vb7ldPI7S&wx_header=0&fontgear=2&scene=27#wechat_redirect)

守护隐私，即刻开始

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9701d03894ec" alt="" />

---


### [安技汇](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxMTM4MTM2MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxMTM4MTM2MQ==)

[:camera_flash:【2022-10-29 23:59:44】](https://mp.weixin.qq.com/s?__biz=MzkxMTM4MTM2MQ==&mid=2247483919&idx=1&sn=4b15f3341cee06719b3ed3c31c6ad6ba&chksm=c11c4411f66bcd07b272bae2897bb00933f98069dc43b214967144fe8a1a10c66380c8a391a7&scene=27#wechat_redirect)

智能安全运营、ATT&amp;CK知识图谱开源落地、安全防护能力评估

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1bddcfd317ee" alt="" />

---


### [长亭百川云平台](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjgyNDIzOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjgyNDIzOA==)

[:camera_flash:【2023-10-08 12:05:31】](https://mp.weixin.qq.com/s?__biz=Mzg4MjgyNDIzOA==&mid=2247510808&idx=1&sn=0a2dc982e3a3bdf56e75c6a120d3d8b0&chksm=cf522e59f825a74fb5d5f31f33fd06c13ab0265c4fbb5ee69330dd76f7312ba29c72d5af00df&scene=27#wechat_redirect)

百川云平台（Rivers）是长亭面向企业开放的在线安全产品服务，包含了多个安全产品，如问脉容器安全产品，关山WebShell检测产品，牧云主机安全产品，以及其他第三方安全公司提供的安全产品等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b7b2cd336ce7" alt="" />

---


### [四维创智](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzI1MDg2Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzI1MDg2Mg==)

[:camera_flash:【2023-10-30 17:30:34】](https://mp.weixin.qq.com/s?__biz=MzIwMzI1MDg2Mg==&mid=2649943595&idx=1&sn=cdcb0bcaf9eb681f3e0e292990583504&chksm=8ed403eab9a38afc85e3f9994f5ed7456ef0881dfb285a751b9aef24016990f97acd38b7c483&scene=27#wechat_redirect)

主营业务涵盖应用安全、系统安全、运维安全等，包括IT资产管理、智能攻击机器人、智能防御机器人、攻防实战演练支持、安全培训、课题和安全技术研究、产品研发及服务综合解决方案提供；其中，行业解决方案又包括电力行业、金融行业、军工行业、公检法等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_10621ea3bed3" alt="" />

---


### [创宇安全智脑](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNjU0NjAyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNjU0NjAyNg==)

[:camera_flash:【2023-10-26 15:52:20】](https://mp.weixin.qq.com/s?__biz=MzIwNjU0NjAyNg==&mid=2247487663&idx=1&sn=a6acb7cd2d1becc63c9976d7c10579a9&chksm=971ea5aaa0692cbce9055211ec57c5b04b58e694ddf9c5162df51829de50db808a05398b155e&scene=27#wechat_redirect)

基于知道创宇14年来海量真实攻防数据积累，通过AI+安全大数据能力持续自生产、精粹和分析，实时输出高精准、高价值威胁情报，赋能于全场景安全建设体系。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_dd8b72c1c127" alt="" />

---

