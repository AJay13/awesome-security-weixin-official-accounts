
### [XCTF联赛](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5NDU3MjExNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5NDU3MjExNw==)

[:camera_flash:【2023-10-31 09:00:21】](https://mp.weixin.qq.com/s?__biz=MjM5NDU3MjExNw==&mid=2247514863&idx=1&sn=f1f290b55cafc32a8d743831781d5320&chksm=a68748d591f0c1c3cab51b6be479c137e95e4f809cfa72e2007a0ed2467f5e3f21fff04d8ec9&scene=27#wechat_redirect)

国内最早、亚洲最大的网络攻防联赛。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3d7c7f90f79f" alt="" />

---


### [胖哈勃](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2OTUzMzg3Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2OTUzMzg3Ng==)

[:camera_flash:【2023-03-15 18:20:45】](https://mp.weixin.qq.com/s?__biz=MzI2OTUzMzg3Ng==&mid=2247501413&idx=3&sn=652b24d4dbe04cfe0b21b0893a97f246&chksm=737a0a911a8b510be6ab7c0dae265be48773c285c6d5207f3f4c0fcc20778bb8b9345b650c1a&scene=27&key=98b23746bc435060df8a55f28843d482f23bde5fff7d69be07e6eee3e9362584c4fef6defc216c121c087880dea8679f621df76014f42d5f31a91e96ea5a2e3505e8dd164a5a6bb1e5a1f885bd0c6221c137e79689221eb0818972dac9d4eff05e11158dd65bd59b86b75edebb75820138eb45929b14d35bf5b3ccd974b771f5&ascene=15&uin=MzgxODQ4MjMz&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_66ccbb1db194&countrycode=GY&exportkey=n_ChQIAhIQD8H4TCslKE5rPoJ4Kglr7xLvAQIE97dBBAEAAAAAADfyB2%2BOYw8AAAAOpnltbLcz9gKNyK89dVj0GW6ZZqcPNpVJhfhQRix2mn1eLiWKcyYDpK0vBPJ%2F2yzjvfkBq50ypMGm537TN52Wx0dCzd1LYY2e61Sbbcfn2Q1FfDacto4OkcNJPnrBq2pILRMGqT7b5pewqoBspWRXDQPeWf%2BmsfO2hwJIjRZEgaAzlM02Uvf4M%2Fl8wafiR79bGD09qYv7GXA9%2FHYfWoJe4Pf4R3R4mN1HafvbqTGZxfc3NMW02b4HRkrJ7I%2BBnC6ThaadU1WY7qAFdMUh7hbdV9NkFx6PFKEY&acctmode=0&pass_ticket=Hoqzs5dUYcUmxS4Kq&scene=27#wechat_redirect)

1990年，哈勃望远镜（Hubble Space Telescope）发射升空，开启了人类对宇宙空间的崭新探索。 现在，Pwnhub的出现，将引领那些对网络安全感兴趣的人们探索“0 1”世界中的无限奥秘。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_2e9e965bad75" alt="" />

---


### [PTEHub](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NzY5NjgyNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NzY5NjgyNw==)

[:camera_flash:【2023-10-24 08:02:39】](https://mp.weixin.qq.com/s?__biz=Mzg4NzY5NjgyNw==&mid=2247484674&idx=1&sn=e108a4bc58a5bb111304625c07dd8c42&chksm=cf8739d6f8f0b0c04ec52ddf0446124cd93c3f3e0735f75ec8c6a968e599ad1df7969115e04b&scene=27#wechat_redirect)

PTE小技巧

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5aff651a75ac" alt="" />

---


### [DataCon大数据安全分析竞赛](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5Njg1NzMyNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5Njg1NzMyNw==)

[:camera_flash:【2023-10-27 18:20:51】](https://mp.weixin.qq.com/s?__biz=MzU5Njg1NzMyNw==&mid=2247487402&idx=1&sn=0c409c88ff4694bc06fb10dd50be1034&chksm=fe5d172ac92a9e3cdeac20925c7a3dfd54e74f956daac60b3b6a01ef2afb0df17d554b7b3c3e&scene=27#wechat_redirect)

由奇安信集团、清华大学、蚂蚁集团主办，联合北京大学、中科院软件所、复旦大学、中山大学、山东大学、中国科技大学等30多个知名高校和研究机构共同举办的聚焦大数据安全分析的比赛。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a0316d342599" alt="" />

---


### [寰宇卫士](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzU0NDY5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzU0NDY5OA==)

[:camera_flash:【2023-07-24 14:54:19】](https://mp.weixin.qq.com/s?__biz=MzIwMzU0NDY5OA==&mid=2247497328&idx=1&sn=cd3214b5fdd5c2ba4270917ac3ea4171&chksm=96cf7765a1b8fe73ecee3c6d9f554aef44026e10cb15165a9064b1671106ec041b840439a389&scene=27#wechat_redirect)

寰宇卫士综合安全咨询、安全服务、解决方案、认证培训、产品研发等信息安全多元化产业链为一体。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7aa3785c2fbe" alt="" />

---


### [春秋伽玛](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNDA5NjgyMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNDA5NjgyMg==)

[:camera_flash:【2023-10-10 19:38:21】](https://mp.weixin.qq.com/s?__biz=MzkyNDA5NjgyMg==&mid=2247496589&idx=1&sn=a2010189891f17f85d5ce5ce55c9df78&chksm=c1d9b3d4f6ae3ac2b6c7f11d1c48c7adbc2c89c45e6299f86ede72b3d56935a84b73cb5f2a6e&scene=27#wechat_redirect)

春秋伽玛，为广大网络安全爱好者传递网络安全大赛相关赛事信息，提供优质的赛事服务平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_07fa2c2720be" alt="" />

---


### [r3kapig](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MDE4MzkzMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI2MDE4MzkzMQ==)

[:camera_flash:【2023-08-16 09:55:51】](https://mp.weixin.qq.com/s?__biz=MzI2MDE4MzkzMQ==&mid=2247484527&idx=1&sn=8dcd1348f14b1c97c9fb7bc954e72287&chksm=ea6cc67bdd1b4f6d8cb1c35e69dfdcc1a51f78682f89b91fae7b1900d11cea092a9a7cbb52d3&scene=27#wechat_redirect)

power by r3kapig team

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4d1d402cbd9d" alt="" />

---


### [CTF之家](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3OTI3ODY2Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3OTI3ODY2Mw==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

CTF，从入门到放弃

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e09d3ecadafe" alt="" />

---


### [TencentCTF](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3NzY2NzAyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3NzY2NzAyNg==)

[:camera_flash:【2018-10-26 22:52:19】](https://mp.weixin.qq.com/s?__biz=MzU3NzY2NzAyNg==&mid=2247483671&idx=1&sn=371dabc2238ce5c0acbfc345eeff1894&chksm=fd006baaca77e2bc06db42a638b0637b4c1472382236d71b90cbe48042c06a2a0db1b3498173&scene=27#wechat_redirect)

腾讯信息安全争霸赛(Tencent Capture The Flag，简称TCTF)由腾讯安全发起，腾讯安全联合实验室主办，腾讯安全科恩实验室承办，0ops安全团队和腾讯安全玄武实验室协办，致力于联合行业伙伴建立国内首个专业安全人才培养平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d70902412103" alt="" />

---


### [中学生CTF](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MzEwMTQ3NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MzEwMTQ3NQ==)

[:camera_flash:【2023-10-28 11:07:57】](https://mp.weixin.qq.com/s?__biz=MzU3MzEwMTQ3NQ==&mid=2247507042&idx=1&sn=48e21edee1d85614fef8fcf3f9714928&chksm=fcc458aecbb3d1b8be37bac2be83c92b87f938dd14535040aeba44e715e511c868355dc982df&scene=27#wechat_redirect)

青少年CTF（原名中学生CTF）帮助大家学习CTF竞赛，信息安全与网络技术。大家如果喜欢我们的视频讲解和教程可以加入我们的QQ群：797842833。快来和我们一起学习安全知识吧。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0d0714849484" alt="" />

---


### [ctffish](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NTMyOTY0Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NTMyOTY0Mg==)

[:camera_flash:【2021-11-01 11:33:00】](https://mp.weixin.qq.com/s?__biz=MzI4NTMyOTY0Mg==&mid=2247483882&idx=1&sn=e81e7c5e03f0ae6cffdbfba07667a257&chksm=ebec906fdc9b19796c0a6cdbc1c989375b0c8bc271caefc1256162fc88890c511d51b510cee1&scene=27&key=8820c3cc18af110b0fce9918d0061a140803d08fccc67167d73a6710e238eb386c397f43652283710bf78156cd985ca664a712a5ea0cad4476a4aa02a41c239c10ab448bf13e7011af56bec81a5a974f020f8efb9c7ea08597e11bd23b19a8b04d19bd1f58dab4a6b65d17ac8a988547a8b8cbefa5055390f13d73c6e6e9ec0b&ascene=15&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=63060012&lang=zh_CN&session_us=gh_67c88f900352&exportkey=AXvh5XvKjVOr38SD9ngk1xM%3D&acctmode=0&pass_ticket=FUIX%2BPHUaY3bYYrLVa2YcWqGU6NZc2q5l%2BC6OBUUBbpKPaURSma2njDkspNZjbSC&wx_header=0&fontgear=2&scene=27#wechat_redirect)

ctf技术交流。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9c7e2109cda3" alt="" />

---


### [ctf大赛](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNTAzNTQ2Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNTAzNTQ2Nw==)

[:camera_flash:【2018-07-23 22:46:38】](https://mp.weixin.qq.com/s?__biz=MzUxNTAzNTQ2Nw==&mid=2247483682&idx=1&sn=597f1e30e4e1c2d133e35d468cfe781d&chksm=f9bd9906ceca10105d2d94c8f406de66aae2b878a753a95f223d9c0ece11e8d02f9b7a72a68c&scene=27#wechat_redirect)

不定期转载一些优秀的ctf资料，希望能够一起学习，越走越远。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_30f9d5eb026a" alt="" />

---


### [青少年CTF](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNDUzODQzMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNDUzODQzMQ==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

面对青少年群体，分享和传播信息安全知识，普及和倡导信息安全精神

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9209145652e2" alt="" />

---


### [CTF黑客呓语](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3MTk3MTQ0NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3MTk3MTQ0NQ==)

[:camera_flash:【2018-01-25 17:09:45】](https://mp.weixin.qq.com/s?__biz=MzI3MTk3MTQ0NQ==&mid=2247483680&idx=1&sn=e6f7150dd4f7ce732c13103569d06d6c&chksm=eb38e43adc4f6d2cc9abc75f1b00342bd70e0943bc7b7e5e42f5b18cc5f0dd91553c30f55337&scene=27#wechat_redirect)

关于安全的个人心得和梦呓

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9dd733d806d2" alt="" />

---


### [CTFCookbook](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MzAwMjEwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2MzAwMjEwNg==)

[:camera_flash:【2018-09-06 22:14:00】](https://mp.weixin.qq.com/s?__biz=Mzg2MzAwMjEwNg==&mid=2247483691&idx=1&sn=999d87b558dfd992ae169f7857c487ad&chksm=ce7e0dbef90984a8306810fd1672b84d8f1452c14c51188285d590e02e762f5065bcafc9e45e&scene=27#wechat_redirect)

个人学习日记/随笔

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_01f5b6370014" alt="" />

---


### [妖灵今天做CTF了吗](http://wechat.doonsec.com/wechat_echarts/?biz=MzU1NjUyNTMxNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU1NjUyNTMxNA==)

[:camera_flash:【2018-06-17 21:02:04】](https://mp.weixin.qq.com/s?__biz=MzU1NjUyNTMxNA==&mid=2247483663&idx=1&sn=c49957ca0356c047fa9fb541378ffd99&chksm=fbc2f362ccb57a7411d61855cbc44fbe9ee42076e1fcc36fbed24d79239a20cfdb5284b4fa13&scene=27#wechat_redirect)

就不告诉你

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4a5d80f6a1b4" alt="" />

---


### [2020集大网络安全竞赛](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MDg4NDUwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1MDg4NDUwNg==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

2020集美大学计算机工程学院网络安全趣味竞赛

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_89a752e2800d" alt="" />

---


### [强网竞赛](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjY3NDQ2Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjY3NDQ2Ng==)

[:camera_flash:【2023-04-18 10:19:15】](https://mp.weixin.qq.com/s?__biz=Mzg4MjY3NDQ2Ng==&mid=2247486413&idx=1&sn=72669437a6ad7f3cfc74bfdbe8309e8a&chksm=cf525580f825dc96eaf67f2280a8e4798ff550a837c0841f5b6e0b652511db758356395cc2e6&scene=27#wechat_redirect)

强网竞赛是面向高等院校和国内信息安全企业的国家级赛事，旨在通过高对抗的竞技比赛，发现锻造网络安全领域优秀人才，提高国家网络安全保障能力，提升全民网络安全意识。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_cf8bfd7de794" alt="" />

---


### [XPLAN](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDY1ODU2Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDY1ODU2Mw==)

[:camera_flash:【2023-03-15 10:21:02】](https://mp.weixin.qq.com/s?__biz=Mzg5MDY1ODU2Mw==&mid=2247492716&idx=1&sn=978f902330b0cde6d888a9542f84942d&chksm=cfdbee00f8ac6716427ff64d436b9c614e6c75dd8b29ef4d7c8f497d5f143726851b156176c4&scene=27#wechat_redirect)

X是未知，更是无限，邀您一道创造、见证和实现X-Plan，北斗逐光，安全有恒，未来无界，共创数字安全未来！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b0a5876d02ea" alt="" />

---


### [SCUCTF](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNDk1ODUxMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxNDk1ODUxMw==)

[:camera_flash:【2023-10-19 10:10:06】](https://mp.weixin.qq.com/s?__biz=MzUxNDk1ODUxMw==&mid=2247486229&idx=1&sn=ad252ebda510a673d5abe9291e7bfd2d&chksm=f9bcbecbcecb37ddd8814a08d15038fc8ac867da2f44f970b8a36c344e3f118d1dd5486d4e92&scene=27#wechat_redirect)

CTF协会

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1e4280acde68" alt="" />

---


### [BugKu](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3ODQ2MDMxMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3ODQ2MDMxMg==)

[:camera_flash:【2023-08-11 16:43:51】](https://mp.weixin.qq.com/s?__biz=MzU3ODQ2MDMxMg==&mid=2247486771&idx=1&sn=a2a1abf8eedcee3a430c7c943765befd&chksm=fd7442c4ca03cbd2d53c535f7b21237932e59fb90f6ec55b542489df644dc2ee418f623816fb&scene=27#wechat_redirect)

关注网络安全，和你一起分享安全技术。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d10dfd684f1b" alt="" />

---


### [山海之关](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjcxMTAwMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MjcxMTAwMQ==)

[:camera_flash:【2023-10-31 11:23:11】](https://mp.weixin.qq.com/s?__biz=Mzg4MjcxMTAwMQ==&mid=2247487654&idx=1&sn=d02ba234aa0f3050658c577c8a9c5fd5&chksm=cf53d010f82459066708ccdb963b6ea0c7b434963ecd2754b8f8d6b02ee18373bb9801a27eb1&scene=27#wechat_redirect)

山海关安全团队公众号。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f11d07068d45" alt="" />

---

