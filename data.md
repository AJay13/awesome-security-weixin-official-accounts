
### [谈数据](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NzYwNTMzNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1NzYwNTMzNw==)

[:camera_flash:【2023-10-29 07:30:57】](https://mp.weixin.qq.com/s?__biz=MzI1NzYwNTMzNw==&mid=2247518443&idx=1&sn=71944f233bd66a2e481bc642eeefedbf&chksm=ea16235ddd61aa4b7c6ed6d52a49e408e2bae4040f8c8d9b7b73f56abfb34b37997fdeafc4c7&scene=27#wechat_redirect)

聚焦数据治理，数字化转型，数据中台等领域专业知识总结和实战分享，做你身边最有价值的数据号！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e1508c424fae" alt="" />

---


### [数据安全治理实践](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjMzNjA3MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjMzNjA3MQ==)

[:camera_flash:【2023-01-16 15:34:21】](https://mp.weixin.qq.com/s?__biz=MzkzMjMzNjA3MQ==&mid=2247483836&idx=1&sn=add747b8c6351665b8f693ac9dd9ebed&chksm=c25c1914f52b9002389347518b3ba02a2736d1c841e340926ce4d24d88db80959a2561f4a53f&scene=27&key=c31f59d5cc9183c58967e8c44c7b816fd2b4afdba8157e827f33b3eee000ef6b8a08b7694eba38cf320d9dcb2cf1242a98ebd38d8e676d9ba059fa09c770db543f6d1c21658de48018ffb56545915a34adc656d8271c4fde632f5375ee3971ea3a2317e69fef1bedd95f9265f77b49331c88ac2f8346a5825bd8126d8914de44&ascene=15&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=6308011a&lang=zh_CN&session_us=gh_2f63c25b28a9&exportkey=n_ChQIAhIQQHe3HazLreWbh5h0JxAxSBLuAQIE97dBBAEAAAAAAJ0eB1qkoe8AAAAOpnltbLcz9gKNyK89dVj0e7%2BxPi8DKRLY70hrmGLObyd5j%2FA6I8vmNg%2Fa4pQ3zFjrvpz2tBOHIFUihmjaQrIYrvFl9kkQEORWAgUas%2Foo5ka09adI6mcG5VEHf7tM%2FDAcoPTq%2Fijf2fXsqTFnSqVMjit3e%2FVcEIVbu5ZtyHUnsglMFAnH8h2zxY9bfwjFd7kcnG9EYQP6I0aHm5HgtAzBYZu4j8icvc7e8WPNwSIVF587iNA78bQalDEDGAbYtfb59yC8RtINBvWGygUJeBaHqbreFklaMk0%3D&acctmode=0&pass_ticket=gdPH1SRqNoTbc26EuCUbtsh2jT&scene=27#wechat_redirect)

专注于数据安全治理实践，我们将定期与您分享数据安全治理等相关领域内的最佳实践思路与行业洞见，从而让您更好地了解数据安全的重要性。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_3553072b6cda" alt="" />

---


### [商业智能研究](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzg5MTI0OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwMzg5MTI0OQ==)

[:camera_flash:【2023-10-31 21:01:09】](https://mp.weixin.qq.com/s?__biz=MzIwMzg5MTI0OQ==&mid=2247531034&idx=1&sn=267abe6c3834ce56685b048e07dd8b73&chksm=96ca4d44a1bdc4523552db9733c484d6d339ded67ae4d2f40a10d646f299d80f7d119baaee95&scene=27#wechat_redirect)

专注于企业数据化应用、大数据BI技术和理论观点的研究，致力于让数据成为企业真正的生产力！帆软数据应用研究院旗下账号。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_7c85d5aeb85d" alt="" />

---


### [中伦文德网络安全与数据合规](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNDgyNTg0NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNDgyNTg0NQ==)

[:camera_flash:【2023-10-25 18:01:16】](https://mp.weixin.qq.com/s?__biz=MzIxNDgyNTg0NQ==&mid=2247491641&idx=1&sn=ca44c5c0c0c1a124b17e280edf83250d&chksm=97a30e23a0d487359c275fae49e515490c3fea920383054c58b56bec0e1c836e78d3aa751370&scene=27#wechat_redirect)

中伦文德网络安全与数据合规专委会

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4b33f9a2e329" alt="" />

---


### [数据安全治理技术](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNzk2OTQ4MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNzk2OTQ4MQ==)

[:camera_flash:【2023-10-31 18:21:46】](https://mp.weixin.qq.com/s?__biz=MzAwNzk2OTQ4MQ==&mid=2247484182&idx=1&sn=23668e1bfa79fa63b418c6ec70905e93&chksm=9b774cb0ac00c5a6b47a4f849a7c548c32365a90731878e9cff2cf6c20d2755251cd55a564e4&scene=27#wechat_redirect)

聚焦数据安全治理，研究数据安全产品及相应技术实践

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_871cd2d145db" alt="" />

---


### [网络安全与数据治理](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzODk1NzY5NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzODk1NzY5NA==)

[:camera_flash:【2023-10-26 13:26:45】](https://mp.weixin.qq.com/s?__biz=MzIzODk1NzY5NA==&mid=2247495547&idx=1&sn=d5f428fbc86e3ec144c40f93d56e6492&chksm=e933ca6ade44437cbbf63437996b2b79b2ac75fee66965afbffc878bfc8bae7e1f2ac1952fce&scene=27#wechat_redirect)

www.pcachina.com

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ddbdee0c5caf" alt="" />

---


### [信安协数据安全与隐私计算专委会](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzODMwODc0OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzODMwODc0OA==)

[:camera_flash:【2023-10-13 17:24:32】](https://mp.weixin.qq.com/s?__biz=MzkzODMwODc0OA==&mid=2247483784&idx=1&sn=8767664d525c43edbc57b2e48ccfaa02&chksm=c283646bf5f4ed7d2cb31ac3fd4c9f1aec90115094b074bedfbecf211e2f56e9e73cc426c78b&scene=27#wechat_redirect)

发布行业信息，展示上海市信息安全行业协会数据安全与隐私计算专业委员会工作、活动内容。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f595b3cec559" alt="" />

---


### [DSO首席数据安全官](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MTgwMzA2MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4MTgwMzA2MQ==)

[:camera_flash:【2023-08-18 14:42:34】](https://mp.weixin.qq.com/s?__biz=Mzg4MTgwMzA2MQ==&mid=2247484278&idx=1&sn=b9bd254cbc66e96077196aa344e0318f&chksm=cf61292cf816a03afe2bd0efb38cd11f88fe2c44f8366070c6dc9b597d2350c264b599630798&scene=27#wechat_redirect)

数据安全产业发展趋势，行业数字人才培训孵化。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b41a97e2173c" alt="" />

---


### [数据安全星球](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5ODY5NDQyNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5ODY5NDQyNw==)

[:camera_flash:【2023-09-25 10:05:42】](https://mp.weixin.qq.com/s?__biz=Mzg5ODY5NDQyNw==&mid=2247487257&idx=1&sn=fc024365626a72827b65a49799dced78&chksm=c05fe36df7286a7b5024f72c1638fa8561498ea4f78354d8b1f8e5fcc17993d452d78b2274d0&scene=27#wechat_redirect)

聚焦数据安全，资讯热点、行业研究、知识科普、前沿技术…尽在数据安全星球！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1b04de438534" alt="" />

---


### [杭州数据安全联盟](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyMjI2MDE2Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyMjI2MDE2Mg==)

[:camera_flash:【2023-10-27 21:05:53】](https://mp.weixin.qq.com/s?__biz=MzkyMjI2MDE2Mg==&mid=2247501317&idx=1&sn=9f8298534b886b998f363b53f70df310&chksm=c1f581a8f68208be59d28f440372cdf01662d3eecc6a5f2c6a6a6bec24eef23af7df6880b9f1&scene=27#wechat_redirect)

杭州数据安全联盟是由杭州市数据资源管理局业务主管和指导的社会团体组织。本订阅号作为杭州数据安全联盟新闻宣传、资源发布和信息共享的公众平台，实时发布杭州数据安全相关信息，助力数智杭州发展。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4959e361d8ba" alt="" />

---


### [未言数据安全中心](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyMTMwMzc1Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyMTMwMzc1Mw==)

[:camera_flash:【2023-10-20 10:00:10】](https://mp.weixin.qq.com/s?__biz=MzkyMTMwMzc1Mw==&mid=2247484882&idx=1&sn=67a99ead6657b5b0e0ab5a7b3b4e700a&chksm=c184ea74f6f3636249dd47e801312f6d732eaa5f774825d895ad33b9a6ad94ed4bf87e92bed8&scene=27#wechat_redirect)

关注数字化发展中的数据安全问题，热点事件时评，数据安全政策法律法规分析，全球数据合规、个人信息保护立法动态，企业数据合规实践。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_68243d752302" alt="" />

---


### [中国数据安全产业网](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2OTczNjMwNQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2OTczNjMwNQ==)

[:camera_flash:【2023-10-09 13:52:54】](https://mp.weixin.qq.com/s?__biz=Mzg2OTczNjMwNQ==&mid=2247487199&idx=1&sn=395e1e46c05768f949e017dea64e6dd9&chksm=ce99c4c1f9ee4dd71ba52374388101315db5cb0b3cae1c42d3ba3706c8e99591f7f7a9389708&scene=27#wechat_redirect)

发布国内数据安全产业动态信息，包含新闻资讯、政策文件、发展现状、趋势态势等。设置中国数据安全产业大会、数据安全产业发展成果展、数据安全创新大赛和数据安全产业培训等专栏，实时更新发布系列活动最新内容与进展。链接工信部数据安全相关公共服务平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a30bfee064c9" alt="" />

---


### [大数据安全工程研究中心](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Nzg0NzUxOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Nzg0NzUxOQ==)

[:camera_flash:【2023-09-28 14:20:46】](https://mp.weixin.qq.com/s?__biz=MjM5Nzg0NzUxOQ==&mid=2247486799&idx=1&sn=d8e5be2c1fc69c37fe5e1a402413016d&chksm=a6d285ff91a50ce91c6291a819ec04a2bee19dbcd35df27f8d0c662f17c1242c7e41868eaebe&scene=27#wechat_redirect)

提供大数据安全设计咨询、实时推送前沿数据安全技术、解读数据安全标准；为政企单位提升信息安全意识，提供数据安全解决方案。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e09832f580ea" alt="" />

---


### [数据安全与数据要素化研究](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNzQyMTEyMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNzQyMTEyMQ==)

[:camera_flash:【2023-10-30 10:11:06】](https://mp.weixin.qq.com/s?__biz=MzIwNzQyMTEyMQ==&mid=2247488150&idx=1&sn=2853cdd4180ee4b7c08f40c176e5eb72&chksm=9713f938a064702eadf6f4875be12ce9c93953cc1da8985704a903fd08651b477582f39a9e96&scene=27#wechat_redirect)

聚焦数据安全与数据要素化，洞察行业趋向，提供专业解读，分享研究成果。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_287b260a46c9" alt="" />

---


### [区块链数据安全存储](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0MzE1NDA3OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI0MzE1NDA3OA==)

[:camera_flash:【2022-05-05 23:27:06】](https://mp.weixin.qq.com/s?__biz=MzI0MzE1NDA3OA==&mid=2650741633&idx=2&sn=6b35de753f64771a9a0c0cad38e4fa37&chksm=f17ad670c60d5f66fb85408927f7623462af55df1ca2fb46feba9bef9b46110110efa70989eb&scene=27&key=d8725908e28fa3745a1d18e98d1af4e69a23e9d4651312d322e0a729178cddd0e6410dffc132dad9a6bfe7a5e7e47a7d2b3bf24334ff0193a589382f1c14231718cf6245d9f1c9a22aa50cd079e0893b77293521a2dd72ee4b4d075fca14f21f3ac7f877a0ed39ce14f61ea6e0869762455d5f1e7fee442164811fc09069015c&ascene=15&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=63070517&lang=zh_CN&session_us=gh_2e79a3cd6844&exportkey=AWIJwejvxy9Wb95to1UaCRQ%3D&acctmode=0&pass_ticket=Ymjv1Pfn4Am5OVs62I3Gh6Ho9wCY9POu%2FNPwp5Pnc3vwel1T62bdPxon38VYk53A&wx_header=0&fontgear=2&scene=27#wechat_redirect)

舵手云获公安部“国家信息系统安全等级保护”非银行机构最高等级等保三级备案认证，标志着系统安全性已达到行业内最高标准，可为客户提供高安全可靠的技术服务，分布式存储技术获得北京2021年创客大赛三等奖，参与国家战略“东数西算”，落实国家新基建

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_97bf06e2bb1e" alt="" />

---


### [数据安全服务](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMDM1MDgzNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzMDM1MDgzNA==)

[:camera_flash:【2023-07-07 17:20:00】](https://mp.weixin.qq.com/s?__biz=MzIzMDM1MDgzNA==&mid=2247486780&idx=1&sn=2d15280be88f54dbd94412dfc54cd9e6&chksm=e8b587a1dfc20eb74e60ef6a8e7f6d35a07067d798844fdbd5b22eef09b0198dfcc2973c6815&scene=27#wechat_redirect)

浙江省数据安全服务有限公司

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4b08a9c66ef8" alt="" />

---


### [天锐数据安全](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5MTk0MzIzMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5MTk0MzIzMQ==)

[:camera_flash:【2023-10-31 08:22:50】](https://mp.weixin.qq.com/s?__biz=MjM5MTk0MzIzMQ==&mid=2652017869&idx=2&sn=7de738cc4f40c9d86e89680f78fd61e4&chksm=bd4beb9d8a3c628b746b95d7d0552fa81bf7ec7a32b6a4650686e0b6de1e958804d41e4f1f8a&scene=27#wechat_redirect)

可信赖的数据安全产品与服务提供商

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a112d631fa8f" alt="" />

---


### [天喻数据安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxMzIwMTE0MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxMzIwMTE0MQ==)

[:camera_flash:【2023-10-23 17:30:34】](https://mp.weixin.qq.com/s?__biz=MzIxMzIwMTE0MQ==&mid=2651899100&idx=1&sn=18457017a39da6e24138824d075692cf&chksm=8c5e1d46bb29945058ac629011e070d7d974ea594e1f21662959ba43e76b76b04b0a9a518a56&scene=0&xtrack=1&key=debaf56e0df4198f72d645653b870d8a727223fbd4e8baccd537f8a1074916054e0b9daab063d9908648cb813ee3775fa388c6c230e2b2203cbe0de8047aa2b48876a9a0fe7a56edccbe7f0bc985589e83413b02780f730e4ad5c3c7ca17a978324abf9ee83a597a8e963c95f2d971f8b192f04cc81ff50c45072bb1197a1d26&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_be29c56ed812&countrycode=AL&exportkey=n_ChQIAhIQAue3I1IoOCQcgsFqX6Ih6xLuAQIE97dBBAEAAAAAALtSMud3DmIAAAAOpnltbLcz9gKNyK89dVj0sj4Gzh5hpGlc5WtvpdFRIp438iyhPozb8o7s7MraPfS6Ltab6Jy9sbJ6oPdzxkK753Kl4E%2BZsA%2F9Jx2C%2FRl6djsv9L1P%2B3m57Bfka907293DKtGEEr%2Fh8NFaUmjIay16t%2Bxz7KRsBKLjMdy6JpL2s%2FdSTNc8%2BTV5K9Wy6q0hj3EbNeR2q%2BRMH20BdxxI8aCxQNilJw7CQ6%2B5l%2FeYq5VWOxw5t2kyvddNXALq312Y8GOT8QDDGr9d1EunGovYJtUCqHlSmGwuG%2BA%3D&acctmode=0&pass_ticket=5uoMfxAlhNyLLaz0d3Hodz7XcX1YRE6yFGsJAiVmlfPpV9mj8y6CqXUTEuGhsmjJ&wx_header=0&fontgear=2&scene=27#wechat_redirect)

总有些关乎安全的知识值得分享。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_be29c56ed812" alt="" />

---


### [数据安全治理专委会](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NjE5NjUxOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NjE5NjUxOA==)

[:camera_flash:【2023-10-13 11:42:11】](https://mp.weixin.qq.com/s?__biz=Mzk0NjE5NjUxOA==&mid=2247485409&idx=1&sn=6a403408ce0830dc093938ecdc189c8e&chksm=c3089ea8f47f17bebb87bade5ae101085ec7fca8082e47040c0bbc842bde30adf2d9f1176264&scene=27#wechat_redirect)

“浙江省网络空间安全协会数据安全治理专委会”公众号，分享数据安全政策法规、行业资讯、技术研究、会员动态。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_45f2171baa79" alt="" />

---


### [数据安全域](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzM0NzA3Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzM0NzA3Ng==)

[:camera_flash:【2023-10-24 09:47:22】](https://mp.weixin.qq.com/s?__biz=MzkyNzM0NzA3Ng==&mid=2247518015&idx=1&sn=bd908cad1a4fb8b60f214e32a6e67224&chksm=c22bbfd4f55c36c2ca0f45b2c86d521be6fb8ab530e13f17334e7e93d04742e40deba83264eb&scene=27#wechat_redirect)

聚焦数据安全领域内最新产业动态信息，包含新闻资讯、政策文章、发展现状、趋势态势等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e078f72a9a06" alt="" />

---


### [数据安全共同体计划](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDcxODc5NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDcxODc5NA==)

[:camera_flash:【2023-10-25 18:00:42】](https://mp.weixin.qq.com/s?__biz=Mzg5MDcxODc5NA==&mid=2247489478&idx=1&sn=091ca03ede18fb304332ed3d5cbf9344&chksm=cfd908e3f8ae81f548485f87f4a4c3a29483147f412637d0186af5b00d38ab84890e41162620&scene=27#wechat_redirect)

“数据安全共同体计划”为了促进《数据安全法》《个人信息保护法》落地实施，推动数据开发利用和数据安全领域的技术推广和产业创新，致力于促进数据安全产业链各环节的交流与合作，推动数据安全政策、技术、人才多要素良性互动，构建数据安全产业生态共同体。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_385b203e9e03" alt="" />

---


### [数据安全研究院](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0ODIxNjA1MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0ODIxNjA1MA==)

[:camera_flash:【2023-05-04 17:05:30】](https://mp.weixin.qq.com/s?__biz=Mzk0ODIxNjA1MA==&mid=2247486567&idx=1&sn=d0e2afe6aff41e34a2db347cc7ce3116&chksm=c36a45faf41dccec7415af5923989b0dce27401bc34fdcfad5025a9d6908caac3ef3a38feef8&scene=27#wechat_redirect)

“哈工大（深圳）-奇安信数据安全研究院”（简称数据安全研究院）由哈尔滨工业大学(深圳)和奇安信集团联合成立。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_66f2d6fee2d3" alt="" />

---


### [CCIA数据安全工作委员会](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzI3MzAxOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNzI3MzAxOA==)

[:camera_flash:【2023-10-26 17:04:32】](https://mp.weixin.qq.com/s?__biz=MzkyNzI3MzAxOA==&mid=2247512444&idx=1&sn=026b338e3cbd25a134ae04e122a9fc5b&chksm=c2286829f55fe13f4425e58a56b6506fb6f048e64cac6ccd70330f5c229d1189e9dc920fdd05&scene=27#wechat_redirect)

围绕数据安全和个人信息保护，解读规则、推广标准、积淀实践、研究热点、沟通大众，以求真务实、科学中立的态度，做我国数据安全发展的观察者、记录者、倡导者。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_2b92aaea64ea" alt="" />

---


### [数据安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxOTc4MjY3MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxOTc4MjY3MQ==)

[:camera_flash:【2023-10-12 09:35:37】](https://mp.weixin.qq.com/s?__biz=MzAxOTc4MjY3MQ==&mid=2247483769&idx=1&sn=e905d081358c93dbc6dfe6b327b4c64d&chksm=9bc08c19acb7050fb21b725cb0badf43eeea18c2bb113f412e83f25c917c0090676d8419401d&scene=27#wechat_redirect)

数据安全与隐私保护圈子，旨在研究各种最新的数据安全与隐私保护技术，探讨在企业的落地之道。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a2ac126da534" alt="" />

---


### [数据安全推进计划](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NjY3MDE3MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NjY3MDE3MA==)

[:camera_flash:【2023-10-24 15:39:51】](https://mp.weixin.qq.com/s?__biz=Mzg3NjY3MDE3MA==&mid=2247489658&idx=1&sn=2407224f70eae67ee879a78b4e96e69e&chksm=cf2fe5cff8586cd908675f0d8ec10f57b326d124479748524c29af79ec4efd198b0538f62408&scene=27#wechat_redirect)

围绕数据安全政策解读、数据安全标准建设、数据安全评估评测、数据安全咨询服务、数据安全人才培训认证等内容搭建数据安全交流平台，构建数据安全专业社群。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5e558917e916" alt="" />

---


### [安华金和](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3MTQwNTQxMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3MTQwNTQxMg==)

[:camera_flash:【2023-10-30 14:22:23】](https://mp.weixin.qq.com/s?__biz=MzA3MTQwNTQxMg==&mid=2650785612&idx=1&sn=2de22fd6fd096b856725b259ecfdd74e&chksm=87257d47b052f451a47890d414934b810dc60b0fd8f8c102d6ca2b1cdab661a31f1e9bf09909&scene=27#wechat_redirect)

中国专业领先的数据安全产品、解决方案及咨询服务提供商。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_25eab0459e95" alt="" />

---


### [数安行](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5OTM0NTM2OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5OTM0NTM2OQ==)

[:camera_flash:【2023-10-27 18:01:07】](https://mp.weixin.qq.com/s?__biz=Mzg5OTM0NTM2OQ==&mid=2247490443&idx=1&sn=1d7c7df21326d24e130a6331d1ec4753&chksm=c055e53df7226c2b5101a9dd98d9f70956b5867b9e301688ef81ed131bd0e5e386a4d860a01b&scene=27#wechat_redirect)

数安行，专注数据运营安全，让数据安全地创造价值！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4a91b25696f1" alt="" />

---


### [全知科技](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0NDMxODY2Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU0NDMxODY2Nw==)

[:camera_flash:【2023-10-26 17:20:26】](https://mp.weixin.qq.com/s?__biz=MzU0NDMxODY2Nw==&mid=2247521133&idx=1&sn=d49f596f337446aff68b77b6c2280d4f&chksm=fb7f26c9cc08afdf6bc2c72695e2d75d826846676926feb57b04af5fa18914e7426dbe02986a&scene=27#wechat_redirect)

数据在流动，可见才安全

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0bd30f1b0430" alt="" />

---

