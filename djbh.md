
### [天億网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4ODU1MzAyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4ODU1MzAyNg==)

[:camera_flash:【2023-10-23 13:55:10】](https://mp.weixin.qq.com/s?__biz=MzU4ODU1MzAyNg==&mid=2247512950&idx=1&sn=53fb3d285f6f5d20bfa8c14249421cb6&chksm=fdd9fd5bcaae744d597ce76521e4ee0e08b5916c975ed550d1a07964e50f47d94ce85a87e0c0&scene=27#wechat_redirect)

全面介绍网络安全相关知识、安全建设方案、分享网络安全行业法律法规及相关政策等内容，一个学习网络安全知识、技术、业务交流的全国性平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_22fb33f67c7e" alt="" />

---


### [安全内参](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NDY2MDMwMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NDY2MDMwMw==)

[:camera_flash:【2023-10-30 17:56:46】](https://mp.weixin.qq.com/s?__biz=MzI4NDY2MDMwMw==&mid=2247510139&idx=1&sn=34622edd2a938db36585def9c891f6ae&chksm=ebfaef5bdc8d664dd03a70b92ae35671b2eef3a0e825e50aa4c335680aef1dfd2ac566d05db9&scene=27#wechat_redirect)

《安全内参》是专注于网络安全产业发展和行业应用的高端智库平台，致力于成为网络安全首席知识官。曾用名“互联网安全内参”。投稿&amp;合作请邮件联系 anquanneican#163.com

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_737fbe06535a" alt="" />

---


### [老马玩工控安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzODU0MzIxNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzODU0MzIxNA==)

[:camera_flash:【2022-03-19 14:49:40】](https://mp.weixin.qq.com/s?__biz=MzIzODU0MzIxNA==&mid=2247484878&idx=1&sn=21460dd55978d8f880baa63a47352e1d&chksm=e9368dcfde4104d92ddf51f5f6fe12aa1eab689f10f09fb9cc14696e9d457a15be26848f39f9&scene=27&key=b58103fa9e1debe48ad61fc32d8ce32b6d900344fc441ae973280ca21183a786c5f9396fd12644d1a0833eea44e94213c0b741c98ef4fe7a20588613ee92f20336c4fe424456a67a574da4e941274f0f6a98505ebe8657d0ea1542d1d4edfa81b872bd1b14e433f7560ba829ccad3c31b24a867644d11e15f4bc672a527c4d49&ascene=1&uin=MTM1NzU2MDQ1OQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=A%2BTU4erFlUrE3BfXriHpQjY%3D&acct&scene=27#wechat_redirect)

聚焦工控安全，提供集政策标准、工具、论文等免费下载以及技术交流、互动、交友为一体的工控安全交流平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e41f6c29c07a" alt="" />

---


### [威努特工控安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNTgyODU3NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwNTgyODU3NQ==)

[:camera_flash:【2023-10-30 08:01:06】](https://mp.weixin.qq.com/s?__biz=MzAwNTgyODU3NQ==&mid=2651103487&idx=1&sn=989545b3203bb7912c7193a96532cb74&chksm=80e6ba4fb7913359b6b5997a32057128df9692235c74741d0c4361d631eb73cbf1c38ba704ad&scene=27#wechat_redirect)

我们将为您分享最前沿的国际工控网络安全技术，国家相关政策法规解读及经典成功案例解析。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b6b192e50c70" alt="" />

---


### [网络安全等保测评](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3MDY0Nzg1Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI3MDY0Nzg1Nw==)

[:camera_flash:【2023-10-31 20:28:35】](https://mp.weixin.qq.com/s?__biz=MzI3MDY0Nzg1Nw==&mid=2247488389&idx=3&sn=f6b2be400b5c913178644ff4a350f786&chksm=eacc84dbddbb0dcd81a8e10123fe6abb59a7c1e794802d755a0ccc5202881ee1df9c1ee83f42&scene=27#wechat_redirect)

学习笔记和资讯分享。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_630452116db5" alt="" />

---


### [保密基](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMzE5MzMwOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMzE5MzMwOQ==)

[:camera_flash:【2021-10-16 10:59:24】](https://mp.weixin.qq.com/s?__biz=MzkzMzE5MzMwOQ==&mid=2247485015&idx=1&sn=41914177586915a4686391aacb1e7c84&chksm=c2517565f526fc73d05bf94e4fcf8e0614bc761973f0c1a41027e241bf51279d28cca82eadf8&scene=27#wechat_redirect)

(等)保(商)密(关)基ㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤx7fx7f等级保护2.0测评交流，问题探究，最新标准分享，提供自建网盘分享资料

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4633a1748cc8" alt="" />

---


### [信息安全国家工程研究中心](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5OTQ0NzY3Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5OTQ0NzY3Ng==)

[:camera_flash:【2023-11-01 00:01:24】](https://mp.weixin.qq.com/s?__biz=MzU5OTQ0NzY3Ng==&mid=2247495262&idx=2&sn=a757bac3512acb55e8ad2c0d6e440108&chksm=feb66f4dc9c1e65b24f22decdc0ad0dee31c9c56794a0ac14563b9b897c587aefd4361b508b8&scene=27#wechat_redirect)

由国家发改委于2004年批复、2012年授牌，法人实体为中科信息安全共性技术国家工程研究中心有限公司。在网络安全服务、体系建设等有丰富的技术研究和工程化经验。依托中科院信工所等科研单位，搭建国家网络安全工程化与产业化平台、产学研创新技术。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1e0d1778d4b2" alt="" />

---


### [祺印说信安](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5MzU5MzQzMA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5MzU5MzQzMA==)

[:camera_flash:【2023-11-01 00:13:53】](https://mp.weixin.qq.com/s?__biz=MzA5MzU5MzQzMA==&mid=2652102426&idx=3&sn=97756d6c16dbcc8a780e3c87723f4252&chksm=8bbcf523bccb7c355927b91dd339e7bb0e1d5ac3c6625593fe25f499bc26d4ca1a7efc2023d6&scene=27#wechat_redirect)

学习网络安全、说网络安全；共同致力于网络安全、网络安全等级保护。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9c4aaf253255" alt="" />

---


### [河南等级保护测评](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NjY2MTI3Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg2NjY2MTI3Mg==)

[:camera_flash:【2023-11-01 00:00:24】](https://mp.weixin.qq.com/s?__biz=Mzg2NjY2MTI3Mg==&mid=2247492570&idx=3&sn=07b61eba5ee2d24b45f3d35e08c90f1e&chksm=ce45c52df9324c3b638d6672c5963d1058a3b99f64832fd1915fe44cb311d402e2088921dc62&scene=27#wechat_redirect)

做对用户有真实价值的网络安全服务，等级保护测评、风险评估、网络安全培训、网络安全咨询、网络安全合规。传播网络安全知识，分享网络安全政策，共建风清气正的网络安全氛围。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f87e87030547" alt="" />

---


### [汇能云安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNzAwOTQxMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNzAwOTQxMg==)

[:camera_flash:【2023-10-31 10:28:30】](https://mp.weixin.qq.com/s?__biz=MzIwNzAwOTQxMg==&mid=2652249399&idx=1&sn=57140b488258dc976b57801a0261d1d8&chksm=8cfa5edcbb8dd7caf566ee12708821cbe447a62ecd9f80a8218e7c7cbf16e8bd9a51e83c3265&scene=27#wechat_redirect)

信息安全等保测评、信息安全服务；云卫士CDS-容灾备份、智能安全网关、智能网络管理产品支持服务；智云iSC-云平台、云服务技术支持服务；朗云iOT-物联网产品支持服务。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5f81484d311e" alt="" />

---


### [e安在线](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1OTA1MzQzNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI1OTA1MzQzNA==)

[:camera_flash:【2023-10-31 09:19:12】](https://mp.weixin.qq.com/s?__biz=MzI1OTA1MzQzNA==&mid=2651244719&idx=1&sn=74c17894c322c32e73334907907aeee1&chksm=f18caabac6fb23ac5a168f27430338f1e5e6edfd35bce6ab0ab8a1b96021e3a4d6a2a2ec934f&scene=27#wechat_redirect)

关注益安在线，一站了解等级保护、工业互联网安全。专注网络安全人才培训，行业咨询、网络安全技术大赛服务。举办2017和2018中央企业网络安全技术大赛、“护网杯”2018网络安全防护赛，为行业提供人才赋能综合解决方案，输送实战型网安人才。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_60355a8dbe4b" alt="" />

---


### [信科共创等保测评](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNTIyMTU1Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNTIyMTU1Nw==)

[:camera_flash:【2023-01-31 17:36:02】](https://mp.weixin.qq.com/s?__biz=MzIyNTIyMTU1Nw==&mid=2247484419&idx=1&sn=a5d33f1dfa978e68b7458900821291cb&chksm=e8024ad9df75c3cfb66d46e5f94d9634cee5be4e6bceb7acd427496d31b6e5fb78a071334106&scene=27&key=c31f59d5cc9183c5a8360b00fcc4ecc8fc731db2ca60bdb2543c8c8164ca51fb170a42d575bd4ad40476c79ace77e6ff655bf214dff4e0e5d0e7d579d963729ab3b45ca21f3b8dc518f78ba11a678dd3c062d47285915176811893df858bae06912f43e093ec8201d6b462fa085365e253b94ef5a01f7839da5e882b18686f3d&ascene=15&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=6308011a&lang=zh_CN&session_us=gh_7730c4cf8f8a&exportkey=n_ChQIAhIQ4gxkbGJU6ENEFS5IQiohxBLuAQIE97dBBAEAAAAAAG%2BkCPvBleAAAAAOpnltbLcz9gKNyK89dVj0SMZPwGY4AiDFNxtQ191Jf46F0VhUXq9xEQE5NBd7V8zhiZbGGUbi%2BDKlyrlgf1FCqt2mJtBrDcpF7ZXnIrjwabggzpKrMkjgiEVu7LmMhCn3mPyhX14pDKYzkKPjS5X7GZ%2FIBfkFT6sWz7x4DtXWaHqTHcChIKITxusiYSteSHZUezyaZGEYvO4LBCZ5P%2Fx3xBv75WvwrtfHz5%2BhD0ZOgN79ayU2C5rC3NqMd3UByl2vX8lSmanucPJAPDIo8o0KXzx%2BT53i1Lc%3D&acctmode=0&pass_ticket=EhTa4NlPWhdndJKK0%2F74noMhqO&scene=27#wechat_redirect)

关于网络安全等级保护的知识、技术、业务的交流平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9dcfc5864bcd" alt="" />

---


### [等保测评整改](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NDU1MzExNQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NDU1MzExNQ==)

[:camera_flash:【2022-06-28 19:11:55】](https://mp.weixin.qq.com/s?__biz=Mzg3NDU1MzExNQ==&mid=2247483940&idx=1&sn=2ea0f94e43a10185c21f2dc3d185c0a1&chksm=cece4850f9b9c146f8f3134c848c7f8db7d31ea290dd919c92402164b45bc2d2da851f3e8520&scene=27&key=58b514bb6e497e7891c2e4f49f01f7fddf57e8d6d7570989028191c11a53a7c782430ee51246c09e462727364d127a61c78581e8f40b11ec1f658585339d115bdafc38b2aaa76071f36b7f805501d8efd8871e1953f679c7c80ed178f44b93c331b064ff00b25c179129270ef9c8733f4cb6c3ac8140d927815e0f00d306e1fa&ascene=15&uin=MTM1NzU2MDQ1OQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6307001e&lang=zh_CN&session_us=gh_99c29b36f837&exportkey=A8i7PgnkTIk30Da7D0sgy38%3D&acctmode=0&pass_ticket=IurvUUxLY8tEsqWQ%2FyWF56%2FlJ6yIyIDjOvjYvi0WuOMEk0v%2F7bW%2BwPbDMvw%2Bw8Ox&wx_header=0&fontgear=2&scene=27#wechat_redirect)

测评整改、渗透测试、主机加固、风险评估、Hvv&amp;重保、系统集成

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_5a49c93db94e" alt="" />

---


### [公安部网络安全等级保护中心](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3NTQwNDYyNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3NTQwNDYyNA==)

[:camera_flash:【2023-10-25 09:10:20】](https://mp.weixin.qq.com/s?__biz=MzU3NTQwNDYyNA==&mid=2247486885&idx=1&sn=91587f696f06ef84881648c6bf9aaac7&chksm=fd22e29aca556b8c716b8369e31700f6a1099badef7d7578d046227f7a094625000d2c29bc4a&scene=27#wechat_redirect)

国家等级保护制度核心机构

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f5f6bf3d09af" alt="" />

---


### [鼎信安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwOTc4MTE4Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwOTc4MTE4Nw==)

[:camera_flash:【2023-10-30 09:02:18】](https://mp.weixin.qq.com/s?__biz=MzIwOTc4MTE4Nw==&mid=2247496711&idx=1&sn=f7e97cc999e2c154bdab9a90c30d8c2d&chksm=976c1caba01b95bd162785e1e14aff6e596ae04f7b934b037d5cd04c74b5c312763df3d45cb9&scene=27#wechat_redirect)

鼎信安全是河南省管国资企业豫信电子科技集团旗下黄河科技集团控股子公司，专注于提供网络安全服务，是国内领先的信息安全等级测评机构。以合规检测类安全服务为核心，聚焦数据安全、安全检测、SaaS安全服务等方向，致力成为全国领先的网络安全服务商。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_47491f174fdf" alt="" />

---


### [湖南金盾评估中心](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNTI0ODcwMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNTI0ODcwMw==)

[:camera_flash:【2023-09-29 08:00:24】](https://mp.weixin.qq.com/s?__biz=MzIyNTI0ODcwMw==&mid=2662128239&idx=1&sn=c114eb159d072c0c0528d2c1c8d8d5eb&chksm=f35aeebfc42d67a9d769099a94edc97788aa3c05d36684bb8709b52cc0c3b870536f4d54db54&scene=27#wechat_redirect)

提供行业安全资讯，打造专业安全服务平台。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_18727cdf9502" alt="" />

---


### [计算机与网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTk4MDE2MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5OTk4MDE2MA==)

[:camera_flash:【2023-10-31 08:13:52】](https://mp.weixin.qq.com/s?__biz=MjM5OTk4MDE2MA==&mid=2655212250&idx=8&sn=3583724da860208adf5de98cb11a8054&chksm=bc84cf358bf3462388c00375836216f0fac0e7323d4eccf027e94623241d09237c834d11b9fb&scene=27#wechat_redirect)

帮助网络安全从业者学习与成长

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_81b7247a0086" alt="" />

---

