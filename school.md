
### [DROPS攻防训练营](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NjE0Mzk2OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NjE0Mzk2OA==)

[:camera_flash:【2020-12-21 12:08:37】](https://mp.weixin.qq.com/s?__biz=MzA3NjE0Mzk2OA==&mid=2648923097&idx=1&sn=ba86d79e8037d7995883a2067850ba3d&chksm=877237d6b005bec0d3ac47029af8700a6eba7fb1f5eda61fedfa1ed418a22595685e2de811c2&scene=27#wechat_redirect)

中原工学院（ZUT）攻防技术团队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_84fc2a89caa2" alt="" />

---


### [安协小天使](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNDU2ODg3Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIxNDU2ODg3Mw==)

[:camera_flash:【2023-10-15 12:30:42】](https://mp.weixin.qq.com/s?__biz=MzIxNDU2ODg3Mw==&mid=2247485641&idx=1&sn=92749dd571881376725304a8b850f9b6&chksm=97a4d20fa0d35b1979328b88ab17ed273d1b333c557df8ad65fedabeeffc68b8fb92b9c85a26&scene=27#wechat_redirect)

杭州电子科技大学Vidar-Team (原信息安全协会HDUISA&amp;网络空间安全协会)  消息助手

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e4ac6bc8fbfb" alt="" />

---


### [清华大学学生网络安全技术协会](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5MjUwOTY4Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA5MjUwOTY4Ng==)

[:camera_flash:【2023-10-07 18:15:51】](https://mp.weixin.qq.com/s?__biz=MzA5MjUwOTY4Ng==&mid=2651804867&idx=1&sn=2fede54714045c29d2f444966d39fc4b&chksm=8b97f05abce0794c471309576239b6abc41a9269affe65ae765df6aecaeea7c232745b342908&scene=27#wechat_redirect)

清华大学学生网络安全技术协会信息推送和资源平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_26798762d3e1" alt="" />

---


### [广外网络安全实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2MTI2NDA5MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2MTI2NDA5MA==)

[:camera_flash:【2023-09-07 21:20:37】](https://mp.weixin.qq.com/s?__biz=MzU2MTI2NDA5MA==&mid=2247483925&idx=1&sn=596be948c371e462de6fdf2879a82b3f&chksm=fc7a26a0cb0dafb65ab9052527596d15e2dc60727c6fcc33e1e29bf5a2d78b1590290d76fd3e&scene=27#wechat_redirect)

广东外语外贸大学网络安全实验室信息推送和资源平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_8176da819e03" alt="" />

---


### [复旦白泽战队](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4NzUxOTI0OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4NzUxOTI0OQ==)

[:camera_flash:【2023-10-30 15:02:52】](https://mp.weixin.qq.com/s?__biz=MzU4NzUxOTI0OQ==&mid=2247487499&idx=1&sn=3173f2c068efa158c7b9620b1f44179d&chksm=fdeb9475ca9c1d6350cdee07433bc31ec2fef73e743de1ed1b5ec1e2e883da561f2c9310166f&scene=27#wechat_redirect)

以复旦大学系统安全实验室学生为主成立的安全攻防战队，分享最新研究成果，交流系统安全攻防领域技巧。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f1bf07da06d8" alt="" />

---


### [蝰蛇信息安全实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NzgyNjUwNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NzgyNjUwNA==)

[:camera_flash:【2023-10-28 15:49:33】](https://mp.weixin.qq.com/s?__biz=MzA3NzgyNjUwNA==&mid=2247490461&idx=1&sn=18a930097deaab1f8800e062fc59add6&chksm=9f4d4b3aa83ac22c2acd37bfe95c879597ca3e7df707bf20f2c144b547b03c7343c83b1abe64&scene=27#wechat_redirect)

湖南农业大学蝰蛇安全实验室

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_638eab2e92c7" alt="" />

---


### [谛听ditecting](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MzQyOTU0Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MzQyOTU0Nw==)

[:camera_flash:【2023-10-29 08:02:50】](https://mp.weixin.qq.com/s?__biz=MzU3MzQyOTU0Nw==&mid=2247490065&idx=1&sn=c0e0efd6366ed1fd24999fb6e57ec331&chksm=fcc09855cbb71143e17c72c1796ab78eaafc526f37ec586bdb3849e67e7fa9f388b69905676c&scene=27#wechat_redirect)

东北大学“谛听”网络安全团队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ce3e0df31e50" alt="" />

---


### [网络安全攻防训练营](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNTc4MzUwNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNTc4MzUwNg==)

[:camera_flash:【2023-05-19 16:16:18】](https://mp.weixin.qq.com/s?__biz=MzAxNTc4MzUwNg==&mid=2247486409&idx=1&sn=01cb0d62034822294147689d19fb792d&chksm=9bff88f4ac8801e2c83b742931f8f799969102e88649f229092a98e08a2fcb66c61baf4817ce&scene=27#wechat_redirect)

平顶山学院攻防技术团队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_21cd5b0aa014" alt="" />

---


### [0w1Club](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MTkzNzk4Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MTkzNzk4Mg==)

[:camera_flash:【2022-07-08 23:10:51】](https://mp.weixin.qq.com/s?__biz=MzU3MTkzNzk4Mg==&mid=2247483943&idx=1&sn=10011c4514d0232302a091fc195a3a0e&chksm=fcd9c7f6cbae4ee0edf6d8a6ef065e517cb3a499c59f4254730355255ce1c9b35e9b32e71a02&scene=27#wechat_redirect)

西安交通大学 0w1Club 新公众号

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_93b04632331b" alt="" />

---


### [0w1网络安全俱乐部](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMzEwMDk3Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzMzEwMDk3Mg==)

[:camera_flash:【2019-03-06 17:27:42】](https://mp.weixin.qq.com/s?__biz=MzUzMzEwMDk3Mg==&mid=2247484055&idx=1&sn=bae1aca502d690049f5ce286e0f73b5d&chksm=faa86254cddfeb42b11d818cc487f845826353cc4c981a4efcfb0ebaae855b0408af42277ff7&scene=27&key=f7cb72e8598687c91dcb7595521a545659ac45e3b76018b3f731f7e09b378a3877614ea3003c08622331da413c854facae329c0d87ed6473afdcb1074d388913b4c56a0ad19a6f515152de083629f1bdecc698bee6fb6efddb05e08af27639012b4225bafb5a7084b6507f1a88360b673db0af25b5fc03fd537277533f305a46&ascene=0&uin=MzgxODQ4MjMz&devicetype=Windows+Server+2016+x64&version=6300002f&lang=zh_CN&export&scene=27#wechat_redirect)

0w1网络安全俱乐部隶属于西安交通大学智能网络与网络安全教育部重点实验室，旨在进行网络安全方面的学习和研究交流。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_20dc9c32f27c" alt="" />

---


### [青科信安](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxMTE4MDMyOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkxMTE4MDMyOQ==)

[:camera_flash:【2023-03-09 16:38:41】](https://mp.weixin.qq.com/s?__biz=MzkxMTE4MDMyOQ==&mid=2247484134&idx=1&sn=375451e8b9a939e39236fbc953d13e9a&chksm=c12155a0f656dcb68b8ec75a63e05a741e15dda231dd572814c8e1dc52b6c8a531b8a87cdde0&scene=27#wechat_redirect)

青岛科技大学信息安全团队

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_244b9c8677de" alt="" />

---


### [山警网络空间安全实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Njc1OTYyNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MjM5Njc1OTYyNA==)

[:camera_flash:【2023-10-23 10:30:01】](https://mp.weixin.qq.com/s?__biz=MjM5Njc1OTYyNA==&mid=2450786565&idx=1&sn=ecd301d5f308bc7e4f5901f6187beb8e&chksm=b104f8228673713458801f4e84e0726a382df97bb09be1ae1750073b40483d9c79812ae35a38&scene=27#wechat_redirect)

山警网络空间安全实验室（简称：网安社）

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_544c8c38eeaf" alt="" />

---


### [TJ网络安全与电子数据取证实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2NTY2MDAyMw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU2NTY2MDAyMw==)

[:camera_flash:【2022-09-07 16:41:34】](https://mp.weixin.qq.com/s?__biz=MzU2NTY2MDAyMw==&mid=2247485013&idx=1&sn=f00fb6925ea85517d0c961a7c3d59331&chksm=fcb9164dcbce9f5b8db0d8f5f6abe0d1e7ee088cf2330b974e2f68321167ff47e94f68f6d302&scene=27#wechat_redirect)

彩虹七号网络安全与电子数据取证实验室成立于2017年5月20日。主要研究方向有网络攻防、电子数据取证、大数据等模块。团队致力于提供更好的学习交流平台，几年来培养出一大批网络安全专业技术人才。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_fe60a47fe531" alt="" />

---


### [布丁的安全小木屋](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMzIwMTE5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMzIwMTE5OA==)

[:camera_flash:【2023-03-02 13:45:51】](https://mp.weixin.qq.com/s?__biz=MzkzMzIwMTE5OA==&mid=2247484767&idx=1&sn=16304e4e42216c36128b775d4d490c8a&chksm=c251511ef526d8082080f2b1700e2f8c9be4c3df7f9f534151cb77ac0dd81dadc01f18e4a6f0&scene=27#wechat_redirect)

安全学习记录

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_1b26e38d0253" alt="" />

---


### [浙商大信息安全小分队](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5NTM0MjA5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI5NTM0MjA5OA==)

[:camera_flash:【2023-06-01 02:19:10】](https://mp.weixin.qq.com/s?__biz=MzI5NTM0MjA5OA==&mid=2247484352&idx=1&sn=c9f5b9e54c676148a2e74ed2831fef80&chksm=ec545abddb23d3abaca3262a7983dbbb552f73079c6d5274c3216b639893252d88f7ef1cc85a&scene=27#wechat_redirect)

发布信息安全技术及相关竞赛信息。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_711d4ce1da87" alt="" />

---


### [三叶草小组Syclover](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzOTg0NjYzNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzOTg0NjYzNg==)

[:camera_flash:【2023-10-23 12:00:28】](https://mp.weixin.qq.com/s?__biz=MzIzOTg0NjYzNg==&mid=2247490401&idx=1&sn=6f4dd7e2882eecc29b9c8d949e1dc1d2&chksm=e9228ee2de5507f4250596e4b2f8fdb9e3ef7343090cc2ccf28230a1ca29e271771676c5290e&scene=27#wechat_redirect)

一个专注于网络空间安全的高校技术团队，成立于2005年3月，主要研究方向有渗透测试、逆向工程、移动安全、安全编程、漏洞利用等；公众号定期推送小组成员原创文章

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_962842f4917b" alt="" />

---


### [NCVTCSTA](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NjUzODYyNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4NjUzODYyNw==)

[:camera_flash:【2023-10-21 22:08:42】](https://mp.weixin.qq.com/s?__biz=Mzg4NjUzODYyNw==&mid=2247484749&idx=1&sn=beab36afe4b4dd4fe65b46903fb5ceb7&chksm=cf996da1f8eee4b73266c0742d978f7749930aa8fd6ae2e3431e6c7cc4d1bf70cf75dc392dc4&scene=27#wechat_redirect)

南宁职业技术学院网络安全技术协会

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ce0885e25658" alt="" />

---


### [山警CyberSec交流小组](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MTY3ODA0OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MTY3ODA0OA==)

[:camera_flash:【2021-10-14 15:11:46】](https://mp.weixin.qq.com/s?__biz=Mzg5MTY3ODA0OA==&mid=2247483735&idx=1&sn=188a73d42e09319105b3aa9ae4631b66&chksm=cfc8fd58f8bf744e043b578c32bb0cb1db99ce3908136c4e8bcd66cab793e4a9ffb25e851a70&scene=27#wechat_redirect)

山东警察学院在校生团队，不定期更新网络安全相关内容，与各位师傅相互学习，相互进步。更多丰富内容请关注公众号：山警网络空间安全实验室。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0ee4069dd7cc" alt="" />

---


### [广工AD攻防实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3Njc3NDQ5OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3Njc3NDQ5OA==)

[:camera_flash:【2023-10-09 13:39:20】](https://mp.weixin.qq.com/s?__biz=MzU3Njc3NDQ5OA==&mid=2247487546&idx=1&sn=c5d92cf8f9d9b575dac9c8210813b901&chksm=fd0f9837ca78112151fd9504549b9a645b6478120f106ae0fb0cba1c6eac8be78406ae3f27d6&scene=27#wechat_redirect)

广东工业大学A&amp;D（攻击与防御）工作室

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_c375cb5a0589" alt="" />

---


### [渭南师院网络信息安全社](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MjI1NzY1NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MjI1NzY1NQ==)

[:camera_flash:【2023-10-17 22:00:44】](https://mp.weixin.qq.com/s?__biz=Mzk0MjI1NzY1NQ==&mid=2247486363&idx=1&sn=c533342ffa9e66ad038e6db6175ca029&chksm=c2c4af03f5b32615ab89168734ef41d947c59c6531fe6156c2945365005ea0c61e0ed32a341a&scene=27#wechat_redirect)

渭南师范学院网络信息安全协会官方账号，为您提供网络安全相关资讯与技术分享。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f4e0f7548c2f" alt="" />

---


### [赛博安全社团](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNDIyNTE0OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyNDIyNTE0OQ==)

[:camera_flash:【2023-10-27 13:01:09】](https://mp.weixin.qq.com/s?__biz=MzkyNDIyNTE0OQ==&mid=2247484804&idx=1&sn=5871b8355ac37a97776a2278388cdb87&chksm=c1d85e96f6afd780564d7710a6943a91150c5d5a95b9b830cd1f360f57421c1728fc641313b4&scene=27#wechat_redirect)

本社团致力于向大家分享交流网络安全实践技术，主要以CTF竞赛、红蓝对抗、渗透测试、知识讲座等形式分享网络安全知识，培养网安人才，积极响应国家建设网络强国的号召，共筑网络安全防线。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ac1289a6ef60" alt="" />

---


### [黑科大CTF网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDUyMjM5NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MDUyMjM5NQ==)

[:camera_flash:【2020-10-26 22:06:00】](https://mp.weixin.qq.com/s?__biz=Mzg5MDUyMjM5NQ==&mid=2247483667&idx=1&sn=d5cd8269b6e44f3bb7593b05cac186ec&chksm=cfda1f67f8ad96716d571ee76696359c5e6ffaa02291d3ddbce8d6f8194aa16d7507bea3cf4b&scene=27#wechat_redirect)

海纳百川，有容乃大，为网络安全爱好者服务。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f556ee1a73cd" alt="" />

---


### [西柚网安](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NjU1MzU3Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzA3NjU1MzU3Nw==)

[:camera_flash:【2022-11-05 11:34:23】](https://mp.weixin.qq.com/s?__biz=MzA3NjU1MzU3Nw==&mid=2247484543&idx=1&sn=1f015b2e2843403b590cc7a2b11dc158&chksm=9f5ec879a829416f73b17c6d7976d939b1412790014febedfe67b131d06dd6fb393d47e05167&scene=27#wechat_redirect)

西柚网安是西安邮电大学网络空间安全学院展示“德育为先，攻防兼备”人才培养理念的新媒体窗口，专注“系统与数据安全”研究生团队网络攻防方面原创技术帖及学习路线，介绍网络空间安全特长班发展情况，推进学科建设，展现西邮网络空间安全学科的培养特色。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b8318b066606" alt="" />

---


### [内工大网络空间安全研究所](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMzM5NDUzNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMzM5NDUzNA==)

[:camera_flash:【2021-12-11 20:57:15】](https://mp.weixin.qq.com/s?__biz=MzUxMzM5NDUzNA==&mid=2247484075&idx=1&sn=fd3fc2ec33a347a2b62f2b238be5187a&chksm=f95490a2ce2319b4cd4b66b7ecfdd47d04caff5ae7044d324b998d03be23d27c357c1e1f3ff6&scene=27&key=5c074e52529293f4a23616912a9d84637dde7dcf58e2413fb3c4f5f93d5aa554dfba8f8643bd2baba33f56e0e45846c98dc5dc9b2465e8473748ba84804920a3b2f874e722cd3e7071d68610b695fcaf94136203a48f7a355bbf2fd27ed4186b9b82d7ec4aaf45ce05a458ac30074882734ef6a5c418d62b6503ee93831b8f66&ascene=0&uin=MTA3Mzc3OTIzNQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=AcFkBcNxkma7V4fWAvREm0E%3D&acctmode=0&pass_ticket=CLipu1oc3Xo23kKaFPk9VMmJWr0KzXLDKmtoNd6o2PRzCklLCrUb3XxUITQ9X3B0&wx_header=0&fontgear=2&scene=27#wechat_redirect)

网络空间安全学科建设研究和人才培养，网络信息安全技术交流合作、信息交流沟通，CTF攻防比赛，风险评估测试与安全咨询。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b4bd721bb0e1" alt="" />

---


### [东塔网络安全学院](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwMzAyMDAwMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkwMzAyMDAwMQ==)

[:camera_flash:【2022-08-15 17:00:45】](https://mp.weixin.qq.com/s?__biz=MzkwMzAyMDAwMQ==&mid=2247503419&idx=1&sn=0ba82da600c5c748b6967ca6d21982a5&chksm=c09e22b5f7e9aba3ce2d6b886a742a63f079dedc97665dc8b82ec45a6a3b39fd6290147d1ca7&scene=27#wechat_redirect)

网络安全服务、技术特训、技术咨询、渗透测试服务

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_cef293eb3618" alt="" />

---


### [鸿鸟安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzUwOTc1OTQyMQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUwOTc1OTQyMQ==)

[:camera_flash:【2022-08-19 10:16:00】](https://mp.weixin.qq.com/s?__biz=MzUwOTc1OTQyMQ==&mid=2247487687&idx=1&sn=cb0bb285361214516bd5250d174f82be&chksm=f90c0a15ce7b830363a99dd4188a2bddf4ab0f3e113ade5992a91e6dc2a1bc65f0243036c9da&scene=27#wechat_redirect)

鸿鸟只思羽翼齐,点翅飞腾千万里

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_362d1c6237f9" alt="" />

---


### [破壳学院](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4Nzc1MDc3Mw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4Nzc1MDc3Mw==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

破壳学院作为国内少有高品质的白帽黑客技术在线教学平台，我们将采用更为有趣易懂的教学方式，通过“陪伴式”学习，结合精细化的学习追踪服务，帮助学员用最短的时间速度成长，在安全攻防实战中快速找准自己的发展路线。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_2665ffa3a8ae" alt="" />

---


### [攻防世界](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4MDgyNzU5Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU4MDgyNzU5Mg==)

[:camera_flash:【2023-08-17 15:02:55】](https://mp.weixin.qq.com/s?__biz=MzU4MDgyNzU5Mg==&mid=2247485043&idx=1&sn=968354c028b7e6332fc25625dbad455b&chksm=fd51a6d4ca262fc2c9ecc9b63506ceb57920cafd975a329152e1b82b20e7ad50963eb2c1c8cd&scene=27#wechat_redirect)

集学习训练、竞赛实战为一体的网络安全在线能力提升平台

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_92651e33e2ed" alt="" />

---


### [连天教育](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNTk2MDAwNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIwNTk2MDAwNA==)

[:camera_flash:【2023-10-19 09:23:53】](https://mp.weixin.qq.com/s?__biz=MzIwNTk2MDAwNA==&mid=2247487976&idx=1&sn=fb2319a70861476624d70f08fc2bc24f&chksm=9729b7c3a05e3ed5a7eabaa42f8957847a58c208781e40f579a606bb9905d3f07b8ce96b600f&scene=0&xtrack=1&key=094fe642087a4fbed8483491f6be613328d865108646bf6c4f031a4547b06872a65f481d8999447d0de4f3427570434aacce74b1188bc605ea5a0e23ab07c0a075eebc806046dccadbefd1fd86e8fc617aa1f743ee061526ce727b2e97e9ea7f2e945a6377faab81f7a30c160ba2dbe914c1561f5e96df5441ca01839d145e25&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_9fe1a6f307b0&countrycode=AL&exportkey=n_ChQIAhIQkBRAS79bKlU0XuVUSnqSVhLuAQIE97dBBAEAAAAAAO4GDy9meHsAAAAOpnltbLcz9gKNyK89dVj0qHzu1fcX4LRmQ0YwhDya9kHsjUJr%2FIpYu8rx%2B1qq63wft21Ki0M52ovDcwiKIe1tuwx060z6WW8oH0hIHlN8lfO2m74bvMzKqgZc1ACg9uQ9jQywLljBrU2iCPpgjqRr1dpr16D%2Bc8xuwXT06kCOcS3D2qZC603Bhu3LGgWSkwY408h5IOLdbB%2BsJGO9%2B18wFJwh9Yi%2BUxR%2FDveunFEF6XsaqL%2FRcsZ3RHvngh9TYuAf9y0VYD8NDoRIazjlxtvtb7Kpgx7k5%2BI%3D&acctmode=0&pass_ticket=bwNySVq5R2IY8HvNwcueWnk%2FZFwrU7Yx2hfHXCZph8%2B3NZAeN3WnP7qA1Lg%2F83iN&wx_header=0&fontgear=2&scene=27#wechat_redirect)

连天教育，是河南信安世纪科技有限公司旗下独立信息安全培训组织，主要面向国内外信息安全认证和职业技能培训，涉及CISP、CISM、CISSP、CISA、信息安全意识、风险评估、渗透测试、CTF竞赛、Web安全等。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_9fe1a6f307b0" alt="" />

---


### [深圳网安培训学院](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3MDYzMjAyNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3MDYzMjAyNA==)

[:camera_flash:【2023-10-27 10:55:00】](https://mp.weixin.qq.com/s?__biz=Mzg3MDYzMjAyNA==&mid=2247485213&idx=1&sn=fc54233f2cdc291b8bc7d5a4507a85a7&chksm=ce8b982af9fc113cd2f1068137656799f9d072be0a6980d125d839f998403b2c6adc60466f99&scene=27#wechat_redirect)

深圳市网安计算机网络安全培训中心是由深圳市人力资源局备案的社会第三方培训机构。是受广东省人力资源和社会保障厅委托，开展“网络与信息安全管理员”职业技能等级认定工作的第三方社会评价机构。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_94f04a96b1ca" alt="" />

---


### [绿盟科技网络安全学院](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjA4ODM0NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkzMjA4ODM0NA==)

[:camera_flash:【2023-02-04 10:00:36】](https://mp.weixin.qq.com/s?__biz=MzkzMjA4ODM0NA==&mid=2247484943&idx=1&sn=12ccda1429dfff5addbd4a9aceb2bee1&chksm=c2605978f517d06eeaac19f0141772f40de450978d847c0ec527260c3d206d41aabe8c103534&scene=27#wechat_redirect)

绿盟科技网络安全学院（别名：猎鹰学院）秉承”卓尔不凡，极致专业”（The few The professional）的校训精神，依托集团安全研究院、五大安全实验室和四大战队的强大师资力量，提供网络安全人才培养、认证培训以及技术能力提升服务。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_c887198c61eb" alt="" />

---


### [启明星辰网络空间安全教育](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzNDg0NTc1NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzNDg0NTc1NA==)

[:camera_flash:【2023-10-30 14:32:26】](https://mp.weixin.qq.com/s?__biz=MzUzNDg0NTc1NA==&mid=2247503917&idx=1&sn=dcdb1f04be6822687834a43e82d03d41&chksm=fa8c1078cdfb996ea459a46bdd977aa510e09f12d450d6a4904c6f1051be2315cd823f459cc5&scene=27#wechat_redirect)

启明星辰网络空间安全学院以国家网络安全事业发展为使命，依托启明星辰集团多年的技术沉淀及实践积累，提供网络安全人才体系构建与信息安全技术及能力提升教育培训，为建设网络强国提供有力的人才支撑。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_310250868fd4" alt="" />

---


### [合天网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNzU3Mzg2NQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyNzU3Mzg2NQ==)

[:camera_flash:【2023-10-23 17:35:20】](https://mp.weixin.qq.com/s?__biz=MzIyNzU3Mzg2NQ==&mid=2247486830&idx=1&sn=ce57dd70a5f3e5045b36c8174b999c97&chksm=e85e6468df29ed7ea2da373dc67f687134f7e44126dada3b6dab385761d19484823cf479d5a1&scene=0&xtrack=1&key=d842c29fb9ae087a29885d4d5968a8c5a32e0f918152f568e3b9972123c8ab4d9ee4ad1b4a76eafcfa9328ada3dee3ecf500707d9b2c1013002c2927a13af22e06d71b29fa9f6a1ee797bd3cbf3b2ce27407610b61291a6936c84e21ffe92629fa3cf96198a3cb3de9b4ed72cdf4e6855266c5f882e2a01faf7fe53af53fc6ac&ascene=51&uin=NTY2NTA4NjQ%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_a6ba8f92d07d&countrycode=AL&exportkey=n_ChQIAhIQGcqidJWZTe2QGdnj%2BNaglRLuAQIE97dBBAEAAAAAAA56AkKJMzcAAAAOpnltbLcz9gKNyK89dVj0yBIqHgFy93Vnbr8dwoyffGBe%2B0JknhUCwNq5V%2BZLFm7%2BdKoqHgb0oQgysh%2F0VOgfWBrF68oQzCncTnvUOLjJ8%2FxuYC59veg7Z4DDmH0BkTZp0Vedkmrodbg6gU%2FyUYMs4SUep%2FSHHswM7rLeqdGdgNuPYaiPrdMH7GHY7EjgTgCtO8x73Gn7GZfDoNklkAh0rTNZYyqQ4tL6NNLupHoP9tmR22M0zcTWL3Fhl3j%2BqmSrn0KBWHlkLfxlmnTbcneyUdWLdftKoKQ%3D&acctmode=0&pass_ticket=T%2FA61VDUwmVVNcg54gDXtRMIBP%2BIcHmmBc3tEeH9kNETyANr%2FuPlkSzEnxnQcA30&wx_header=0&fontgear=2&scene=27#wechat_redirect)

网安学院拥有专业教学服务、就业指导，课程配套靶场实战平台（www.hetianlab.com），学员学完就练，切实提升实践动手能力；课程涵盖Web安全、软件安全、网络安全等网安各个相关方面。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a6ba8f92d07d" alt="" />

---


### [齐鲁师院网络安全社团](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MDU5NTA1MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU3MDU5NTA1MQ==)

[:camera_flash:【2023-10-25 20:41:22】](https://mp.weixin.qq.com/s?__biz=MzU3MDU5NTA1MQ==&mid=2247497085&idx=1&sn=c9e5002cb9edbd60cab77a45860e4f8a&chksm=fcefb6f9cb983fef1decc49c9922585f8e10354001911d2036821475c9e1a56ffb1c69020144&scene=27#wechat_redirect)

欢迎加入齐鲁师范学院网络安全社团

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_0c9ae293eb8e" alt="" />

---


### [鹏越网络空间安全研究院](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMTQyOTc2OQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMTQyOTc2OQ==)

[:camera_flash:【2021-01-29 14:33:59】](https://mp.weixin.qq.com/s?__biz=MzIyMTQyOTc2OQ==&mid=2247493891&idx=1&sn=dcb5d4fb84643bb8efdd2c61e3d6ae1a&chksm=e83e4065df49c973f5bbf2e4cc7b023b5dc351a9cea095ca34c359c1f7353530a3f7d2146373&scene=27#wechat_redirect)

鹏越是上海交通大学信息安全工程学院产业化平台。致力于全球网络空间安全前沿技术、人才培养、战略规划、网络空间治理、安全资讯、情报分析、人工智能、信息技术前沿等领域的研究，共建网络强国！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_eacadf95533a" alt="" />

---


### [xhCovteam](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5ODE1ODY1OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzU5ODE1ODY1OA==)

[:camera_flash:【2019-11-02 23:59:32】](https://mp.weixin.qq.com/s?__biz=MzU5ODE1ODY1OA==&mid=2247483719&idx=1&sn=83d71f7d83c855863cd882a248d88c84&chksm=fe493d6ac93eb47c262cc9863ff37670e97fd623cab281f7bd16636cd7c18982b4c2820c7953&scene=27#wechat_redirect)

Covteam工作室，成立于2005年。是以西华大学在校本科生研究生为主力，依托于西华大学网络安全实验室，实验室主要负责网络与安全相关内容的研究。培养在网络安全领域全面发展的高级人才的摇篮。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_4fb4ead8c48a" alt="" />

---


### [彼岸花安全团队](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MDI0MTQzNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0MDI0MTQzNA==)

[:camera_flash:【2021-06-11 22:08:24】](https://mp.weixin.qq.com/s?__biz=Mzk0MDI0MTQzNA==&mid=2247483688&idx=1&sn=8349b15f63c755930a16e98a1046123f&chksm=c2e5e65df5926f4b750bb928563ac64c6437571e05458ade5da83ed7879618ad4e810b15502d&scene=27&key=c2c9190ae823cbb9b4c1cdaf77dd8f494141bc0244d3a93d139c9f4498d6f16b39fc5637d3618f8963c402b24cc19d9fbf53b3f6a2fca124e81e7d01119b0e06fd54bc2a684ffbb492b52400a6c7b2d74931a7fc242d8d887e4d506df6b0b26e6eae7ed5071c78446315d51e92e64dc8ca896a7956e336f8fdbd8e03e054676b&ascene=0&uin=MTM1NzU2MDQ1OQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6305002e&lang=zh_CN&exportkey=Ay4bBoKevZrZPSbrSBgnYyg%3D&acctmode=0&pass_ticket=nDZrCbVJzEN19v7O3jTjmzVPWmxH9zPoYj3eCBUDyjuNMRtD82lrGziIz1t3syiY&wx_header=0&fontgear=2&scene=27#wechat_redirect)

安全技术研究及安全技术分享

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f5f71c9883af" alt="" />

---


### [河北网络安全高校联盟](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4ODUyMjYyNg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg4ODUyMjYyNg==)

[:camera_flash:【2023-08-09 09:12:27】](https://mp.weixin.qq.com/s?__biz=Mzg4ODUyMjYyNg==&mid=2247486443&idx=1&sn=c2d659d472d8c85445d8172f5be6d8d7&chksm=cff89906f88f10106d6bad934ac49ab8734464b1179391a019679518260340c4a435acf138d9&scene=27#wechat_redirect)

河北网络安全高校联盟

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_8937984657cd" alt="" />

---


### [杭师大网安](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MzA1MDIzMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MzA1MDIzMg==)

[:camera_flash:【2023-10-19 13:29:17】](https://mp.weixin.qq.com/s?__biz=Mzg5MzA1MDIzMg==&mid=2247494585&idx=1&sn=49284b9c222d9b2dbd995d9d6774058a&chksm=c03667aef741eeb8b5f5a7eea5a0481000285766456e9321ab71fb36671647677191e082c929&scene=27#wechat_redirect)

为了宣传杭州师范大学“网络与信息安全实验室”，提升大家网络与信息安全意识。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_cb6e613490cd" alt="" />

---


### [网络攻防创新实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNDcyMDM1MQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIzNDcyMDM1MQ==)

[:camera_flash:【2023-10-24 09:59:52】](https://mp.weixin.qq.com/s?__biz=MzIzNDcyMDM1MQ==&mid=2247483998&idx=1&sn=8ecd478655975c3384d3f9a91bf642ea&chksm=e8f35f6edf84d678c7687b453bfa9cd2c889046838f01561ab7d57480ce11be21b42e3c658fd&scene=27#wechat_redirect)

原名为网络安全学生社团，现名为网络攻防创新实验室

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_f587b147a938" alt="" />

---


### [软微网络安全](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMzE0NzA2Ng==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzIyMzE0NzA2Ng==)

[:camera_flash:【2022-03-30 07:00:00】](https://mp.weixin.qq.com/s?__biz=MzIyMzE0NzA2Ng==&mid=2247484321&idx=1&sn=332167de6ce8ebb4126db9f392ce3008&chksm=e823e6f4df546fe2e462b68fe762397f333272099d2f423b741e0f69af7330994bf6d6471363&scene=27&key=351cd3e52a9e1e14f8eabc6dcd45848ce46084dac2d306e7a2786e7d51c7eb02a146a1081808293c2cfa7f4a7c204f7b008cd9bb509645d3fea41bfe1e46ff947dbf679c391c1f4b219b3fb89fb8de2ca6fd4db3b2aa6f21c53452be62ee7c7db57161832378e8a832ec3206449a10dc48b4a5c28040926dee7bed98cf9128f2&ascene=15&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6307001e&lang=zh_CN&session_us=gh_5ed70c726e70&exportkey=AyBODOsPLZsIEajbrZOAKHE%3D&acctmode=0&pass_ticket=E6D9rxobWdo83z5JbZwTXiq3vE%2FfRPAifWM1fGfUzbs2CiZCYP%2B6KdGUfkS4DcFz&wx_header=0&fontgear=2&scene=27#wechat_redirect)

隶属于北京大学软件与微电子学院网络软件与系统安全系，小组成员主要来自于北京大学

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b42eef33cb12" alt="" />

---


### [网络安全高校联盟](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NDE3MzIxOQ==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzk0NDE3MzIxOQ==)

[:camera_flash:【未知】](http://wechat.doonsec.com&scene=27#wechat_redirect)

河南CTF高校联盟官方公众号

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d47c1310baaa" alt="" />

---


### [白帽网络安全空间](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4MDEzODYwMg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4MDEzODYwMg==)

[:camera_flash:【2021-09-16 22:16:09】](https://mp.weixin.qq.com/s?__biz=MzI4MDEzODYwMg==&mid=2247483934&idx=1&sn=c12e2751fb31c1b1d1a37e3936ce3e9e&chksm=ebbc441bdccbcd0d52ddefcb9153dde4b6e3dce9e04c07cb49a30cdeb52425121632e77b49e9&scene=27&key=2ef3f3b1a43fbd05095b9a9ec7eeb2bc94508734f31e5303fbaf798c9f49d36e5e20ffe0a8365eabdf16917da34ad01c4eb4169aca23b4b2132721b942da4e0f481f45c9e9428bddd8fd830087021ad6861b9cd962550c2716fd0d5a2efa75ae05646ec8f9847489ee001820590bf34eaa3e4a08feaaec9a6f02c4d99b714109&ascene=15&uin=NTY2NTA4NjQ%3D&devicetype=Windows+Server+2016+x64&version=6307001e&lang=zh_CN&session_us=gh_41292c8e5379&exportkey=A4Cse0iciABwAo0fFZGXn1U%3D&acctmode=0&pass_ticket=E6D9rxobWdo83z5JbZwTXiq3vE%2FfRPAifWM1fGfUzbs2CiZCYP%2B6KdGUfkS4DcFz&wx_header=0&fontgear=2&scene=27#wechat_redirect)

致力于分享白帽网络安全技术，培养网络安全人才，维护网络安全！

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_15c32fd7d860" alt="" />

---


### [中南极光网安实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NjYxNDk3Mg==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzI4NjYxNDk3Mg==)

[:camera_flash:【2023-03-14 08:44:21】](https://mp.weixin.qq.com/s?__biz=MzI4NjYxNDk3Mg==&mid=2247484421&idx=1&sn=d7afda5d680099bd877f2d42cdfc9f53&chksm=ebdb70d6dcacf9c09e05ddbd3aaab753d7e652a507032a0931cb1c452b882e4f8bdaf701b3cb&scene=27#wechat_redirect)

中南极光网安Lab欢迎你的到来~Lets&#39;s hack something!

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e613df22a0fc" alt="" />

---


### [NISL实验室](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMTEwOTA3OA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUxMTEwOTA3OA==)

[:camera_flash:【2023-10-25 15:04:16】](https://mp.weixin.qq.com/s?__biz=MzUxMTEwOTA3OA==&mid=2247485488&idx=1&sn=2a5ecfff0f39853360dcda9a5c15110d&chksm=f979fa89ce0e739f1e37542cb6783d75ffce603f903f5a5c5aa2845e89f6a42e0e0f8bd5376f&scene=27#wechat_redirect)

网络与信息安全实验室(NISL@THU)，专注于网络、系统、应用、人工智能安全教学与研究，在国际四大安全会议发表三十余篇论文，成果在业界产生了广泛影响力。孕育了蓝莲花、紫荆花等知名战队，发起了网安国际学术论坛InForSec。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_52677a4545d1" alt="" />

---


### [大学生网络安全尖锋训练营](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzODkwMDMxNA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzUzODkwMDMxNA==)

[:camera_flash:【2023-10-27 09:00:25】](https://mp.weixin.qq.com/s?__biz=MzUzODkwMDMxNA==&mid=2247570818&idx=1&sn=9efcc9256968839015607b0de10c6441&chksm=fad337b7cda4bea1cc9f713a930ea2e0c90c4768f7a18d206bff1b7b9e6aa8332d42c03f8d75&scene=27#wechat_redirect)

百所高校、百家用人单位、百名尖锋导师，携手打造定制化人才培养模式，通过导师制职业规划和封闭式岗位实训，补足知识短板、提高实战能力，培养创新型、实用型网络安全尖锋人才。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_b54960a6ea07" alt="" />

---


### [中学生网安竞赛](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NDQyOTU5MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg3NDQyOTU5MA==)

[:camera_flash:【2021-10-19 16:00:00】](https://mp.weixin.qq.com/s?__biz=Mzg3NDQyOTU5MA==&mid=2247484599&idx=1&sn=7e8363f3f44d6bedcda6b06f5c4f2d53&chksm=ced1a94ef9a6205810d457e901116380690ce084362bfc400d825b9f00bba8c444f52009a2dc&scene=27&key=bc21bcf8af6ac697b1fc6563a90450cc14ea85eec6e8b1e98d4f42cc18223d264711b48b1b6b464a26dcaf11afc911906c8c47a91014bdda6d51affc1e974ab1d6715cad7548bb6f802df455afc82d937d92cbb9f1a6f2d4385d496408147ed3a8ee533d768dca063bab0c35508cadcec28d90a752165df958e46438a8912fb2&ascene=15&uin=MTM1NzU2MDQ1OQ%3D%3D&devicetype=Windows+Server+2016+x64&version=6307001e&lang=zh_CN&session_us=gh_886b94872294&exportkey=A%2BvIOm3mzFDN6uSmZxMXzcg%3D&acctmode=0&pass_ticket=IurvUUxLY8tEsqWQ%2FyWF56%2FlJ6yIyIDjOvjYvi0WuOMEk0v%2F7bW%2BwPbDMvw%2Bw8Ox&wx_header=0&fontgear=2&scene=27#wechat_redirect)

服务于全国中学生网安比赛，提供最新赛事资讯及历年题目精讲

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_d180bc469d9d" alt="" />

---


### [NUC网安研究所](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MzQ3MjE3Nw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MzQ3MjE3Nw==)

[:camera_flash:【2020-10-17 21:47:34】](https://mp.weixin.qq.com/s?__biz=Mzg5MzQ3MjE3Nw==&mid=2247484313&idx=1&sn=4e9bb96fd77635dd6ac1cf5779230e1e&chksm=c02f1f47f75896519561b5ae0f6a0234c5c4c21abf322474385e4000303f4adc90a583742af9&scene=27#wechat_redirect)

在这里你能get到实验室最新资讯以及知识分享、每周授课预告 : )

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ef1f6c5f3dc4" alt="" />

---


### [蚁景网安](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MTU2ODYwNw==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5MTU2ODYwNw==)

[:camera_flash:【2023-08-22 17:10:03】](https://mp.weixin.qq.com/s?__biz=Mzg5MTU2ODYwNw==&mid=2247486561&idx=1&sn=914195408ffa9cd0f70cf6df9cd9a5a1&chksm=1cd56a3986bc7f3f6244b097d848b2f5d5b2aecef5252a81a550a07965b029e9b1c11a89c2ea&scene=27&key=24e036561734eb125e6e074614f39c62f25699b22090f8362baa5996b3dcb3cf08cf1386233c17db2c0abfebe7bccdf3df0a2ca0888f0e4d66115d548952c71b484aab45c4027bdac2afc1c8c3121cd35162c9921d27f1840ca0ceb056b98d98105f6d1848e81ff0d9a613bb743251cea214b8e0db899a4e2b7ead23108ccfe7&ascene=15&uin=MTI5ODM0MTMwNQ%3D%3D&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_469225414d82&exportkey=n_ChQIAhIQHz3E5hriJik0jj2Bscu08BLvAQIE97dBBAEAAAAAAIzRKQQ6hm4AAAAOpnltbLcz9gKNyK89dVj0J1qqtQ4ejXRuy%2BmEgvj2BeIEBKixPdXdTIXWCLDLIV96bQJW13UeRBnpnc6k7jIpSWgs7OIVaaxASdRrF%2FIKaU3XSkGoPX16QYb14IggEyPkzaAF1uZ%2FAVPRx8v%2FBiQX1%2Bz89YT59lxiZudXq5YzcVxtyCCsbcciDvhT4Q%2FW7pAuWPXTJVI0ZgBXyGMs8MKoyQfoMGmxNPVJ1M4x3kSfEpRMYUAs1nQ9PhRf%2BWVlEcJVlFxBiYXy%2Fan6hb47rzZ%2FofoRJnawMZcH&acctmode=0&pass_ticket=R7ty4VMBglDtif%2BuqnHOYB3of5bAu9DgselW1azirUSBMlLsGGYs7Ci92%2BDmwSUw&wx_header=0&fontgear=2&scene=27#wechat_redirect)

分享网安干货技能，致力于实战网安技能人才培养

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_861ca8c2271b" alt="" />

---


### [浙大网安](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5NDczNDc4NA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=Mzg5NDczNDc4NA==)

[:camera_flash:【2023-10-25 14:48:16】](https://mp.weixin.qq.com/s?__biz=Mzg5NDczNDc4NA==&mid=2247493940&idx=1&sn=de832f304b31ecf7326674a7982c73e6&chksm=c019b19bf76e388d819489b65408f9788d142a8a6f281b721bd26805cbdc68b6369c241b16c4&scene=27#wechat_redirect)

浙江大学网络空间安全学院/浙江大学网络空间安全研究中心

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_e603b55057e7" alt="" />

---


### [Birkenwald](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNDQ2NDE1MA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAxNDQ2NDE1MA==)

[:camera_flash:【2023-03-29 18:01:02】](https://mp.weixin.qq.com/s?__biz=MzAxNDQ2NDE1MA==&mid=2247484962&idx=1&sn=98db868f23d1767a7b8ccacf0401c2b1&chksm=9f3cd8f56bc4a16ce18e531b61cd0238be75ff9b5ba1e2ec32ecbf464d4044f4b756bdcce02a&scene=27&key=60da4d39056d42a5c7a951ef1525729f6cdceaabd0005e8775bf43a2612819d178bf6df89b902df3d3af73b3a7c0621346cb322e5f49e3622fbba0b74d293692b5af89832c5f5244ca80a4243e31d945196576bd94e2c8338f4b3b7fa0817309f1f85aad0aefacabb40fd49a471f98ecd26604385991ec9a5fef1de59452abdb&ascene=15&uin=MzgxODQ4MjMz&devicetype=Windows+10+x64&version=63060012&lang=zh_CN&session_us=gh_c028a5199606&countrycode=GY&exportkey=n_ChQIAhIQkruDjuGDTB5AfH3JZN8ZaBLvAQIE97dBBAEAAAAAAMXsAQwdrQ4AAAAOpnltbLcz9gKNyK89dVj0N0OZJqPW3eOFTwk6V6LT4uP%2BUQqeI9C81kPK%2F5DxzHsbjpWx96cElUVG2KaqgBKCjQrz2dJOcwnD7WGVIxCaBmbNW0qRMLNn6gbRslWioO%2BcLWX5gGuwcS8vZmCnL8gczzNa78HeCgRpv9KiIRxSKPVfjG%2BJxpyTsTfuTu%2B%2B7fXw8ijjGLZt1AnbI6%2B0NYbAF%2BiB7p7H%2FTanlLi8trWdA8%2FliM%2Ffe0iKDZCok9I0EekWdPNIudOtsRV7ZRfJn08pBW1R03%2Bks9lv&acctmode=0&pass_ticket=Hoqzs&scene=27#wechat_redirect)

Birkenwald安全团队 --来自哈尔滨理工大学

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_78acfb2bfff7" alt="" />

---


### [珠天PearlSky](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyODMyODMyOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzkyODMyODMyOA==)

[:camera_flash:【2023-10-14 09:05:58】](https://mp.weixin.qq.com/s?__biz=MzkyODMyODMyOA==&mid=2247487494&idx=1&sn=fb1b8accd8edbd8fe5af97e5ed6e5dd4&chksm=c21b2fe1f56ca6f78d6a0e1465c47d6b937486a5bf9553c83b27dffb3a7ef7bafd530f35eb66&scene=27#wechat_redirect)

珠天PearlSky，意为珠科安全的一片天。珠天的队员都来自于珠海科技学院热爱钻研网络安全技术的同学。我们希望我们能够让更多人了解网络安全，更欢迎各位同学加入我们一起交流学习。本公众号会发布一些学习心得，经验总结等等一系列文章。

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_a4b0a923e487" alt="" />

---


### [Stalker安全团队](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwMTI5NDYzOA==)

[:chart_with_upwards_trend:【0/0/30】](http://wechat.doonsec.com/wechat_echarts/?biz=MzAwMTI5NDYzOA==)

[:camera_flash:【2022-08-26 03:04:27】](https://mp.weixin.qq.com/s?__biz=MzAwMTI5NDYzOA==&mid=2247483798&idx=1&sn=691989441cab80cbb194509076ce3d13&chksm=9adaa9d7adad20c1e5e707e06ab42bb860b95f00996f142af9fb71bc09069c1f90951a96fedf&scene=27#wechat_redirect)

江西理工大学信息安全工作室——Stalker战队公众号

<img align="top" width="180" src="http://open.weixin.qq.com/qr/code?username=gh_ef9d824452a5" alt="" />

---

